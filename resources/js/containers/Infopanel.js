import { connect } from 'react-redux'
import { Infopanel } from '../components/Infopanel/Infopanel'
import { logout } from '../store/action/actionAuthorization'
import { withRouter } from 'react-router-dom'


const mapStateToProps = state => ({
    nameUpfr: state.authorization.nameUpfr,
    fioUser: state.authorization.fio,
    descAccess: state.authorization.descAccess
})

const mapDispatchToProps = dispatch => ({
    logout: () => dispatch(logout())
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Infopanel))
