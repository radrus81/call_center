import { connect } from 'react-redux'
import { Reports } from '../components/Reports/Reports'
import { setValueForReport, createReport } from '../store/action/actionReports'


const mapStateToProps = state => ({
    stateForReport: state.Reports,
    upfr: state.directories.upfr,
    access: state.authorization.access
})

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const { upfr } = stateProps
    const { dispatch } = dispatchProps

    return {
        ...stateProps,
        setValueForReport: (fieldName, value) => dispatch(setValueForReport(fieldName, value, upfr[0]['id'])),
        createReport: (values) => dispatch(createReport(values))
    }
}

export default connect(mapStateToProps, null, mergeProps)(Reports)
