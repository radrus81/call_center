import { connect } from 'react-redux'
import { Actionicons } from '../components/Journal/Actionicons'
import { ShowDetailCall } from '../store/action/actionModaldetailcall'
import { ShowModalDeleteCall } from '../store/action/actionModalDeleteCall'


const mapStateToProps = state => ({
    calls: state.journal.calls,
    access: state.authorization.access
})

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const { calls } = stateProps
    const { dispatch } = dispatchProps
    const { idObjectCall } = ownProps

    return {
        ...stateProps, idObjectCall,
        ShowDetailCall: (idObjectCalls) => dispatch(ShowDetailCall(idObjectCalls, calls[idObjectCalls])),
        ShowModalDeleteCall: (idObjectCalls) => dispatch(ShowModalDeleteCall(idObjectCalls, calls[idObjectCalls]))
    }
}


export default connect(mapStateToProps, null, mergeProps)(Actionicons)
