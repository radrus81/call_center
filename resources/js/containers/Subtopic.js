import { connect } from 'react-redux'
import { Subtopic } from '../components/Modals/Modaldetailcall/ComponentsField/Subtopic'

const mapStateToProps = state => ({
    subTopics: state.directories.subTopics,
    topicCall: state.modaldetailcall.dataCall['topic']
})

export default connect(mapStateToProps, null)(Subtopic)
