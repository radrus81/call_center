import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Layout } from "../components/Layout";
import { Route, Switch, withRouter, Redirect } from "react-router-dom";
import Authorization from "./Authorization";
import { autoAuthorization } from "../store/action/actionAuthorization";
import { getSettingsApp } from "../store/action/actionSettings";
import Journal from "../containers/Journal";
import AddCall from "../containers/AddCall";
import Reports from "../containers/Reports";
import Users from "../containers/Users";
import SecretWord from "../containers/SecretWord";
import { ACCESS_USER_SECURITY_INFO, ACCESS_USER_UPFR } from "../enum/enum";

const Routes = ({
    isAuthorization,
    brand,
    access,
    CheckSession,
    getSettingsApp
}) => {
    useEffect(() => {
        CheckSession(), getSettingsApp();
    }, []);

    var routes;

    isAuthorization
        ? access !== ACCESS_USER_SECURITY_INFO && access !== ACCESS_USER_UPFR
            ? (routes = (
                  <Switch>
                      <Route path="/users" component={Users} />
                      <Route path="/reports" component={Reports} />
                      <Route path="/journal" component={Journal} />
                      <Route path="/addcall" component={AddCall} />
                      <Redirect from="/" to="/addcall" />
                  </Switch>
              ))
            : access !== ACCESS_USER_UPFR
            ? (routes = (
                  <Switch>
                      <Route path="/" component={Users} />
                  </Switch>
              ))
            : (routes = (
                  <Switch>
                      <Route path="/" component={SecretWord} />
                  </Switch>
              ))
        : (routes = (
              <Switch>
                  <Route path="/" component={Authorization} />
              </Switch>
          ));
    return (
        <Layout isAuthorization={isAuthorization} brand={brand}>
            {routes}
        </Layout>
    );
};

const mapStateToProps = state => ({
    isAuthorization: state.authorization.isAuthorization,
    access: state.authorization.access,
    brand: state.authorization.titleApp
});

const mapDispatchToProps = dispatch => ({
    CheckSession: () => dispatch(autoAuthorization()),
    getSettingsApp: () => dispatch(getSettingsApp())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Routes));
