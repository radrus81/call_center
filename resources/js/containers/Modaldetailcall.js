import { connect } from 'react-redux'
import { Modaldetailcall } from '../components/Modals/Modaldetailcall/Modaldetailcall'
import { closeDetailCall, showEditField, lossOfFocus, saveEditedCall } from '../store/action/actionModaldetailcall'


const mapStateToProps = state => ({
    fioUser: state.authorization.fio,
    access: state.authorization.access,
    isModalOpen: state.modaldetailcall.isModalOpen,
    idObjectCall: state.modaldetailcall.idObjectCall,
    dataCallInObject: state.modaldetailcall.dataCall,
    tableCallsCellkey: state.modaldetailcall.tableCallsCellkey
})

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const { idObjectCall, dataCallInObject } = stateProps
    const { dispatch } = dispatchProps

    return {
        ...stateProps,
        modalClose: () => dispatch(closeDetailCall()),
        showEditField: (tableCellsCellkey) =>
            dispatch(showEditField(tableCellsCellkey)),
        lossOfFocus: () => dispatch(lossOfFocus()),
        saveEditedCall: () => dispatch(saveEditedCall(idObjectCall, dataCallInObject))
    }
}

export default connect(mapStateToProps, null, mergeProps)(Modaldetailcall)
