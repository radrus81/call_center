import { connect } from 'react-redux'
import { DropDownListField } from '../components/Modals/Modaldetailcall/ComponentsField/DropDownListField'

const mapStateToProps = state => ({
    areaOfDislocation: state.directories.areaOfDislocation,
    counterparties: state.directories.counterparties,
    oldTopic: state.directories.oldTopic,
    topics: state.directories.topics,
    subTopics: state.directories.subTopics,
    typeConsultation: state.directories.typeConsultation,
    resultConsultation: state.directories.resultConsultation,
    upfr: state.directories.upfr
})

export default connect(mapStateToProps, null)(DropDownListField)
