import { connect } from 'react-redux'
import { ModalDeleteCall } from '../components/Modals/ModalDeleteCall/ModalDeleteCall'
import { CloseModalDeleteCall, deleteCall } from '../store/action/actionModalDeleteCall'



const mapStateToProps = state => ({
    isModalOpen: state.ModalDeleteCall.isModalOpen,
    idObjectCall: state.ModalDeleteCall.idObjectCall,
    dataCall: state.ModalDeleteCall.dataCall
})

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const { idObjectCall, dataCall } = stateProps
    const { dispatch } = dispatchProps

    return {
        ...stateProps,
        modalClose: () => dispatch(CloseModalDeleteCall()),
        deleteCall: () => dispatch(deleteCall(idObjectCall, dataCall.idCall))
    }
}

export default connect(mapStateToProps, null, mergeProps)(ModalDeleteCall)
