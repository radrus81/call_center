import { connect } from 'react-redux'
import { ModalHistoryCalls } from '../components/Modals/ModalHistoryCalls/ModalHistoryCalls'
import { closeModalHistoryCalls, copyCall, showModalHistoryCalls } from '../store/action/actionModalHistoryCalls'



const mapStateToProps = state => ({
    isModalOpen: state.historyCalls.isModalOpen,
    isModalClose: state.historyCalls.isModalClose,
    errors: state.historyCalls.errors,
    dataHistoryCalls: state.historyCalls.dataHistoryCalls,
    totalCount: state.historyCalls.totalCount,
    activePage: state.historyCalls.activePage,
    fullNamePensioner: state.DataForCall.fullNamePensioner
})

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const { dataHistoryCalls, fullNamePensioner } = stateProps
    const { dispatch } = dispatchProps

    return {
        ...stateProps,
        modalClose: () => dispatch(closeModalHistoryCalls()),
        copyCall: (idObj) => dispatch(copyCall(dataHistoryCalls[idObj])),
        PageChangeHandler: (page) => dispatch(showModalHistoryCalls(page, fullNamePensioner))
    }
}

export default connect(mapStateToProps, null, mergeProps)(ModalHistoryCalls)
