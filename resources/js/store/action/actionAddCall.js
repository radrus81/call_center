import {
    ADD_VALUE_FIELD_FOR_CALL, INSERT_CALL_SUCCESS,
    SET_USERS_LAST_CALLS_AND_STATISTIC, SHOW_TABLE_SECRET_WORD,
    DATA_SECRET_WORD, SHOW_ERROR_SERVER_TO_TABLE
} from './actionTypes'
import { getCreateDate } from '../../components/Helpers/Helpers'
import { ShowModalErrorServer } from './actionModalServerError'

export const addValueField = (fieldName, value) => {
    return {
        type: ADD_VALUE_FIELD_FOR_CALL,
        fieldName, value
    }
}

export const addCall = (values) => {
    return async dispatch => {
        var data = new FormData()
        for (var key in values) {
            switch (key) {
                case 'dateOfIssue': data.append(key, getCreateDate(values[key])); break
                case 'timeStart': if (values[key] == 0) {
                    data.append(key, parseInt((new Date().getTime() + (new Date().getTimezoneOffset() * 60)) / 1000) - 120)
                } else {
                    data.append(key, values[key])
                } break
                default: data.append(key, values[key])
            }
        }
        data.append('idUser', localStorage.getItem('idUser'))
        data.append('access', localStorage.getItem('access'))
        data.append('codeUser', localStorage.getItem('codeUser'))
        data.append('codeUpfr', localStorage.getItem('kod_upfr'))
        data.append('timeEnd', parseInt((new Date().getTime() + (new Date().getTimezoneOffset() * 60)) / 1000))
        data.append('_method', 'patch')
        try {
            var res = await axios.post('/api/addcall', data)
            dispatch(insertCallSuccess())
            dispatch(setUsersLastCallsAndStatistic(res.data.userLastCalls, res.data.userStatistic))
        }
        catch (error) {
            dispatch(ShowModalErrorServer(error.response.status))
        }
    }
}

export const getUserCallsAndStatistic = () => {
    return async dispatch => {
        try {
            var res = await axios.get('/api/usercalls/' + localStorage.getItem('idUser') + '/' + localStorage.getItem('access'))
            dispatch(setUsersLastCallsAndStatistic(res.data.userLastCalls, res.data.userStatistic))
        }
        catch (error) {
            dispatch(ShowModalErrorServer(error.response.status))
        }
    }
}

export const getSecretWord = (snils) => {

    return async dispatch => {
        dispatch(showTableSecretWord())
        try {
            var res = await axios.get('/api/secretwords/' + snils)
            dispatch(setDataSecretWord(res.data.dataSecretWord))
        }
        catch (error) {
            dispatch(ShowErrorServerToTable(error.response.status))

        }
    }
}



const ShowErrorServerToTable = (codeError) => {
    return {
        type: SHOW_ERROR_SERVER_TO_TABLE,
        codeError
    }
}


const setDataSecretWord = (dataSecretWord) => {
    return {
        type: DATA_SECRET_WORD,
        dataSecretWord
    }
}

const showTableSecretWord = () => {
    return {
        type: SHOW_TABLE_SECRET_WORD
    }
}

const insertCallSuccess = () => {
    return {
        type: INSERT_CALL_SUCCESS
    }
}


const setUsersLastCallsAndStatistic = (usersCalls, userStatistic) => {
    return {
        type: SET_USERS_LAST_CALLS_AND_STATISTIC,
        usersCalls, userStatistic
    }
}
