import { ShowModalErrorServer } from './actionModalServerError'
import { SET_TITLE_APP } from './actionTypes'


export const getSettingsApp = () => {
    return async dispatch => {

        try {
            let res = await axios.get('/api/settings/')
            let dataSettings = res.data.settingsApp
            dispatch(setTitleApp(dataSettings[0].value, dataSettings[1].value))
        }
        catch (error) {
            dispatch(ShowModalErrorServer(error.response.status))
        }
    }
}

const setTitleApp = (titleApp, showFieldPasswordEdit) => {
    return {
        type: SET_TITLE_APP,
        titleApp, showFieldPasswordEdit
    }
}

