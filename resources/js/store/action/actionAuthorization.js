import { LOGIN_SUCCESS, LOGIN_LOGOUT, LOGIN_ERROR, SAVE_DIRECTORIES } from './actionTypes'
import axios from 'axios'
import { ShowModalErrorServer } from './actionModalServerError'


const expirationTime = 3600

export function CheckLoginAndPassword(login, password) {
    return async dispatch => {
        var dataForm = new FormData();
        dataForm.append('login', login)
        dataForm.append('password', password)
        try {
            let res = await axios.post('/api/login', dataForm);
            if (res.data.errorMessage) {
                dispatch(loginErrors(res.data.errorMessage))
            } else {
                setDataToLocalStorage(res.data.user)
                dispatch(loginSuccess(res.data.user))
                dispatch(loadingDirectories())
            }
        }
        catch (error) {
            dispatch(ShowModalErrorServer(error.response.status))
        }
    }
}

export function autoAuthorization() {
    var user = new Object();
    for (let i = 0; i <= localStorage.length; i++) {
        var nameKey = localStorage.key(i)
        nameKey ?
            user[nameKey] = !isNaN(parseInt(localStorage.getItem(nameKey))) ?
                Number(localStorage.getItem(nameKey))
                : localStorage.getItem(nameKey)
            : null
    }
    return dispatch => {
        if (Object.keys(user).length) {
            const expirationDate = new Date(localStorage.getItem('expirationDate'))
            if (expirationDate <= new Date()) {
                dispatch(logout())
            } else {
                dispatch(loginSuccess(user))
                dispatch(autoLogout(expirationTime))
                dispatch(loadingDirectories(user.access, user.kod_upfr))
            }
        } else {
            dispatch(logout())
        }
    }
}

const loadingDirectories = () => {

    var access = localStorage.getItem('access')
    var codeUpfr = localStorage.getItem('kod_upfr')
    return async dispatch => {
        try {
            let res = await axios.get('/api/directories/' + access + '/' + codeUpfr);
            dispatch(saveDirectories(res.data))
        }
        catch (error) {
            dispatch(ShowModalErrorServer(error.response.status))
        }
    }
}

const setDataToLocalStorage = (user) => {
    const userKeys = Object.keys(user)
    userKeys.map((key) => {
        localStorage.setItem(key, user[key]);
    })
    const expirationDate = new Date(new Date().getTime() + expirationTime * 1000)

    localStorage.setItem('expirationDate', expirationDate)
}

const saveDirectories = directories => {
    return {
        type: SAVE_DIRECTORIES,
        directories
    }
}

const loginSuccess = user => {
    return {
        type: LOGIN_SUCCESS,
        user
    }
}

const loginErrors = message => {
    return {
        type: LOGIN_ERROR,
        message
    }
}

const autoLogout = (time) => {

    return dispatch => {
        setTimeout(() => {
            dispatch(logout())
        }, time * 1000)
    }
}

export const logout = () => {

    localStorage.clear()
    return {
        type: LOGIN_LOGOUT
    }
}




