import axios from "axios";
import {
    SHOW_LOADER,
    CALLS_SUCCESS,
    CALLS_EMPTY,
    CHANGE_STATUS_CALL_SUCCESS,
    SET_VALUE_FOR_FILTER,
    SET_FILTER,
    CHANGE_STATUS_CALL_SUCCESS_FOR_USER
} from "./actionTypes";
import { getCreateDate } from "../../components/Helpers/Helpers";
import { ShowModalErrorServer } from "./actionModalServerError";

export function GetCalls(activePage, isFilter, ...args) {
    var createDateFromFilter = "";
    if (args[1]) {
        createDateFromFilter = getCreateDate(args[1]);
    }
    var dataUrl = `?codeUpfrFromFilter=${args[0]}&createDateFromFilter=${createDateFromFilter}&fullNameClientFromFilter=${args[2]}
    &typeContractorFromFilter=${args[3]}&subTopicFromFilter=${args[4]}&noticeFromFilter=${args[5]}&userFromFilter=${args[6]}&statusFromFilter=${args[7]}
    &codeUpfr=${args[8]} &access=${args[9]}&activePage=${activePage}&pageSize=${args[10]}&isFilter=${isFilter}&phone=${args[11]}`;

    return async dispatch => {
        try {
            if (!isFilter) {
                dispatch(setFilter(isFilter));
            }
            dispatch(showLoader());
            let res = await axios.get("/api/calls/" + dataUrl);
            if (res.data.calls.length) {
                dispatch(
                    callsSuccess(
                        res.data.calls,
                        res.data.totalCountCalls,
                        activePage,
                        res.data.totalCountCallsForCurrentDate
                    )
                );
            } else {
                dispatch(callsEmpty(res.data.totalCountCalls));
            }
        } catch (error) {
            dispatch(ShowModalErrorServer(error.response.status));
        }
    };
}

export function ChangeStatusCall(
    idObjectCall,
    idCall,
    newStatus,
    isCallOnlyUser
) {
    return async dispatch => {
        var data = new FormData();
        data.append("id", idCall);
        data.append("status", newStatus);
        data.append("idUser", localStorage.getItem("idUser"));
        data.append("access", localStorage.getItem("access"));
        data.append("_method", "patch");
        try {
            var res = await axios.post("/api/calls/changestatuscall", data);
            if (!isCallOnlyUser) {
                dispatch(ChangeStatusCallSuccess(idObjectCall, newStatus));
            } else {
                dispatch(
                    ChangeStatusCallSuccessForUser(
                        idObjectCall,
                        newStatus,
                        res.data.userStatistic
                    )
                );
            }
        } catch (error) {
            dispatch(ShowModalErrorServer(error.response.status));
        }
    };
}

export function setFilter(isFilter) {
    return {
        type: SET_FILTER,
        isFilter
    };
}

export function setValueForFilter(fieldName, value) {
    return {
        type: SET_VALUE_FOR_FILTER,
        fieldName,
        value
    };
}

const ChangeStatusCallSuccess = (idObjectCall, newStatus) => {
    return {
        type: CHANGE_STATUS_CALL_SUCCESS,
        idObjectCall,
        newStatus
    };
};

const ChangeStatusCallSuccessForUser = (
    idObjectCall,
    newStatus,
    userStatistic
) => {
    return {
        type: CHANGE_STATUS_CALL_SUCCESS_FOR_USER,
        idObjectCall,
        newStatus,
        userStatistic
    };
};

const showLoader = () => {
    return {
        type: SHOW_LOADER
    };
};

const callsSuccess = (
    calls,
    totalCountCalls,
    activePage,
    totalCountCallsForCurrentDate
) => {
    return {
        type: CALLS_SUCCESS,
        calls,
        totalCountCalls,
        activePage,
        totalCountCallsForCurrentDate
    };
};

const callsEmpty = totalCountCalls => {
    return {
        type: CALLS_EMPTY,
        totalCountCalls
    };
};
