import { SET_VALUE_FOR_FILTER, SET_FILTER } from "../action/actionTypes";

const initialState = {
    isFilter: false,
    codeUpfr: 0,
    createDate: "",
    fullNameClient: "",
    phone: "",
    typeContractor: 0,
    subTopic: 0,
    notice: "",
    user: 0,
    status: ""
};

export default function TableFilterReducer(state = initialState, action) {
    switch (action.type) {
        case SET_VALUE_FOR_FILTER:
            return {
                ...state,
                codeUpfr:
                    action.fieldName === "codeUpfr"
                        ? parseInt(action.value)
                        : state.codeUpfr,
                createDate:
                    action.fieldName === "createDate"
                        ? action.value
                        : state.createDate,
                fullNameClient:
                    action.fieldName === "fullNameClient"
                        ? action.value
                        : state.fullNameClient,
                phone:
                    action.fieldName === "phone" ? action.value : state.phone,
                typeContractor:
                    action.fieldName === "typeContractor"
                        ? parseInt(action.value)
                        : state.typeContractor,
                subTopic:
                    action.fieldName === "subTopic"
                        ? parseInt(action.value)
                        : state.subTopic,
                notice:
                    action.fieldName === "notice" ? action.value : state.notice,
                user:
                    action.fieldName === "user"
                        ? parseInt(action.value)
                        : state.user,
                status:
                    action.fieldName === "status"
                        ? parseInt(action.value)
                        : state.status
            };
        case SET_FILTER: {
            return {
                ...state,
                isFilter: action.isFilter,
                codeUpfr: 0,
                createDate: "",
                fullNameClient: "",
                phone: "",
                typeContractor: 0,
                subTopic: 0,
                notice: "",
                user: 0,
                status: ""
            };
        }

        default:
            return state;
    }
}
