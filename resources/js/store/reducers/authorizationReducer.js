import { LOGIN_SUCCESS, LOGIN_LOGOUT, LOGIN_ERROR, SET_TITLE_APP } from "../action/actionTypes"

const initialState = {
    isAuthorization: false,
    login: '',
    fio: '',
    kodUpfr: 0,
    access: 0,
    errorMessage: '',
    nameUpfr: '',
    descAccess: '',
    userId: 0,
    titleApp: 'Call-центр ПФР...',
    showFieldPasswordEdit: 'enabled'
}

export default function AuthorizationReducer(state = initialState, action) {

    switch (action.type) {
        case LOGIN_SUCCESS:
            return {
                ...state,
                isAuthorization: true,
                login: action.user.login,
                fio: action.user.fio,
                kodUpfr: action.user.kod_upfr,
                access: action.user.access,
                errorMessage: '',
                nameUpfr: action.user.nameupfr,
                descAccess: action.user.descAccess,
                userId: action.user.id
            }
        case LOGIN_LOGOUT:
            return {
                ...state,
                isAuthorization: false,
                login: '',
                fio: '',
                kodUpfr: 0,
                access: 0,
                errorMessage: '',
                nameUpfr: '',
                descAccess: '',
                userId: 0
            }
        case LOGIN_ERROR:
            return {
                ...state,
                isAuthorization: false,
                errorMessage: action.message,
            }
        case SET_TITLE_APP:
            return {
                ...state,
                titleApp: action.titleApp,
                showFieldPasswordEdit: action.showFieldPasswordEdit
            }
        default:
            return state
    }
}


