import { SAVE_DIRECTORIES } from "../action/actionTypes"

const initialState = {
    'areaOfDislocation': [],
    'counterparties': [],
    'oldTopic': [],
    'topics': [],
    'subTopics': [],
    'typeConsultation': [],
    'resultConsultation': [],
    'upfr': [],
    'status': [],
    'users': []
}

export default function DirectoriesReducer(state = initialState, action) {

    const allDirectories = action.directories

    switch (action.type) {

        case SAVE_DIRECTORIES:
            return {
                ...state,
                'areaOfDislocation': allDirectories.areaOfDislocation,
                'counterparties': allDirectories.counterparties,
                'oldTopic': allDirectories.oldTopic,
                'topics': allDirectories.topics,
                'subTopics': allDirectories.subTopics,
                'typeConsultation': allDirectories.typeConsultation,
                'resultConsultation': allDirectories.resultConsultation,
                'upfr': allDirectories.upfr,
                'status': allDirectories.status,
                'users': allDirectories.users
            }
        default:
            return state
    }
}
