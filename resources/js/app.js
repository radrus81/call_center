require('./bootstrap');

import 'bootstrap/dist/css/bootstrap.min.css'
import '../sass/app.scss'
import React from 'react';
import { BrowserRouter } from 'react-router-dom'
import ReactDOM from 'react-dom';
import Routes from './containers/Routes'
import { Provider } from 'react-redux'
import { createStore, compose, applyMiddleware } from 'redux'
import rootReducer from './store/reducers/rootReducer'
import thunk from 'redux-thunk'

const composeEnhancers =
    typeof window === 'object' &&
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        }) : compose;

const createStoreWithMiddleware
    = composeEnhancers(
        applyMiddleware(
            thunk
        )
    )(createStore)

const store = createStoreWithMiddleware(
    rootReducer
)
const app = (
    <Provider store={store}>
        <BrowserRouter>
            <Routes />
        </BrowserRouter>
    </Provider>
)

if (document.getElementById('root')) {
    ReactDOM.render(app, document.getElementById('root'));
}