import React from 'react'
import { Col } from 'reactstrap'
import { Radio } from 'react-formik-ui'


export const RadioBtnSecretWordStatisticsAndReportPhone = ({ setValueForReport }) => {
    return (
        <Col sm={8} className="RadioBtnSecretWordStatisticsAndReportPhone">
            <Radio
                name='typeReport'
                className="form-status"
                onClick={(event) => { setValueForReport('typeReport', event.target.value) }}
                options={[
                    { value: 'secretWordStatistic', label: 'Статистика по кодовому слову (в разрезе по структурным подразделениям)' },
                    { value: 'reportJournalPhone', label: 'Журнал учета телефонных обращений граждан' },
                    { value: 'reportSubjectOfRequests', label: 'Информация об организации дистанционного обслуживания граждан' },
                ]}
            />
        </Col>
    )
}
