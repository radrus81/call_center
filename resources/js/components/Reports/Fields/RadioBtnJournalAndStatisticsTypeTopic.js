import React from 'react'
import { Col } from 'reactstrap'
import { Radio } from 'react-formik-ui'


export const RadioBtnJournalAndStatisticsTypeTopic = ({ setValueForReport }) => {
    return (
        <Col sm={8} className="RadioBtnJournalAndStatisticsTypeTopic">
            <Radio
                name='typeReport'
                className="form-status "
                onClick={(event) => { setValueForReport('typeReport', event.target.value) }}
                options={[
                    { value: 'printJournal', label: 'Журнал звонков' },
                    { value: 'oldStatistic', label: 'Статистика по старым темам обращения (в разрезе по структурным подразделениям)' },
                    { value: 'newStatistic', label: 'Статистика по новым темам обращения (в разрезе по структурным подразделениям)' }
                ]}
            />
        </Col>
    )
}
