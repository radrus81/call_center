import React from 'react'
import { Table, Label, Col } from 'reactstrap'
import { Radio } from 'react-formik-ui'


export const RadioBtnСomplaint = ({ setValueForReport }) => {
    return (

        <Col className="radioBtnСomplaint" sm={8}>
            <Radio
                name='typeReport'
                className="form-status"
                onClick={(event) => { setValueForReport('typeReport', event.target.value) }}
                options={[
                    { value: 'complaint', label: 'Жалобы на УПФР' }
                ]}
            />
            <Col sm={6}>
                <Radio
                    name='reportСomplaint'
                    inline
                    className="form-status"
                    onClick={(event) => { setValueForReport('reportСomplaint', event.target.value) }}
                    options={[
                        { value: 'journalСomplaint', label: 'Журнал' },
                        { value: 'statisticСomplaint', label: 'Статистика' }
                    ]}
                />
            </Col>
        </Col>

    )
}
