import React from 'react'
import { Label, Col } from 'reactstrap'
import { Radio } from 'react-formik-ui'
import { SelectField } from '../../Journal/SelectField'


export const RadioBtnStatisticUsers = ({ upfr, setValueForReport }) => {
    return (

        <Col sm={8} className="radioStatisticUsers" >
            <Radio
                name='typeReport'
                className="form-status"
                onClick={(event) => { setValueForReport('typeReport', event.target.value) }}
                options={[
                    { value: 'statisticUsers', label: 'Статистика по специалистам' }
                ]}
            />
            <Col sm={12} >
                <div className="reportForUpfr">
                    <Label sm={3}>Выберите УПФР</Label>
                    <SelectField
                        sm={9}
                        defaultValue={0}
                        defaultValueName="..."
                        fieldData={upfr}
                        fieldName="codeUpfr"
                        setValueForFilter={setValueForReport}
                    />
                </div>
                <Radio
                    name='reportRadioTypeTopic'
                    inline
                    className="form-status"
                    onClick={(event) => { setValueForReport('reportRadioTypeTopic', event.target.value) }}
                    options={[
                        { value: 'newTopic', label: 'Новые темы' },
                        { value: 'oldTopic', label: 'Старые темы' }
                    ]}
                />
            </Col>
        </Col >

    )
}
