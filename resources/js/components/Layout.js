import React from 'react'
import { Header } from '../components/Header/Header'
import Navbar from '../containers/Navbar'
import { Footer } from './Footer/Footer'

export const Layout = ({ children, brand, isAuthorization, logout }) => (
    <>
        <Header
            isAuthorization={isAuthorization}
            logout={logout}
            brand={brand}
        />
        {
            isAuthorization ?
                <Navbar />
                : null
        }
        <main
            className={!isAuthorization ? 'mainLogin' : 'mainPage'}>
            {children}
        </main>
        <Footer />
    </>
)



