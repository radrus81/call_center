import React from 'react'
import './Loader.css'

export default () =>
    <>
        <div className="loaderDark"></div>
        <div className="loader-wrap">
            <div className="lds-spinner"><div>
            </div><div></div><div></div><div>
                </div><div></div><div></div><div>
                </div><div></div><div></div><div>
                </div><div></div><div></div></div>
        </div>
    </>