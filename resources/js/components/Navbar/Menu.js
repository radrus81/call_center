import React from "react";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faNewspaper,
    faChartPie,
    faUsers,
    faPhoneAlt
} from "@fortawesome/free-solid-svg-icons";
import {
    ACCESS_ADMIN,
    ACCESS_USER_OPFR,
    ACCESS_USER_UPFR_ALL,
    ACCESS_USER_UPFR,
    ACCESS_USER_SECURITY_INFO,
    ACCESS_USER_CONTROLLER_OPFR
} from "../../enum/enum";

const accessGroup = [
    ACCESS_ADMIN,
    ACCESS_USER_OPFR,
    ACCESS_USER_CONTROLLER_OPFR,
    ACCESS_USER_UPFR_ALL
];

const links = [
    {
        id: 1,
        name: "Зарегистрировать звонок",
        href: "/addcall",
        iconName: faNewspaper,
        accessGroup
    },
    {
        id: 2,
        name: "Журнал звонков",
        href: "/journal",
        iconName: faPhoneAlt,
        accessGroup
    },
    {
        id: 3,
        name: "Отчеты",
        href: "/reports",
        iconName: faChartPie,
        accessGroup: [
            ACCESS_ADMIN,
            ACCESS_USER_UPFR_ALL,
            ACCESS_USER_CONTROLLER_OPFR
        ]
    },
    {
        id: 4,
        name: "Пользователи",
        href: "/users",
        iconName: faUsers,
        accessGroup: [ACCESS_ADMIN, ACCESS_USER_SECURITY_INFO]
    },
    {
        id: 4,
        name: "Кодовое слово",
        href: "/secretword",
        iconName: faUsers,
        accessGroup: [ACCESS_USER_UPFR]
    }
];

export const Menu = ({ access }) => (
    <ul className="navbar-nav mr-auto">
        {links
            .filter(props => props.accessGroup.indexOf(access) != -1)
            .map(link => {
                return (
                    <li className="nav-item" key={link.id}>
                        <NavLink className="nav-link mr-2" to={link.href}>
                            <FontAwesomeIcon
                                className="mr-2"
                                icon={link.iconName}
                            />
                            {link.name}
                        </NavLink>
                    </li>
                );
            })}
    </ul>
);

Menu.propTypes = {
    access: PropTypes.number
};
