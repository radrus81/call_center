import React from 'react'
import { Menu } from './Menu'
import './Navbar.scss'

export const Navbar = ({ isAuthorization, access }) => (
    <div className="bg-secondary">
        <nav className="navbar navbar-expand-lg fixed-top navbar-dark bg-secondary ">
            <div className="wrap collapse navbar-collapse" id="navbarSupportedContent">
                {
                    isAuthorization ? <Menu access={access} /> : null
                }
            </div>
        </nav>
    </div>
)
