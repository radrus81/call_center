import React from 'react'
import './Infopanel.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons'


export const Infopanel = ({ history, descAccess, logout, nameUpfr, fioUser }) => {

    return (
        <div className="infoPanel">
            <div className="bg-dark">
                {
                    <>
                        <p>{descAccess}: {fioUser}
                            <FontAwesomeIcon
                                title="Выйти"
                                className="ml-2 iconExit fa-lg"
                                icon={faSignOutAlt}
                                onClick={() => {
                                    history.push('/')
                                    logout()
                                }} />
                        </p>
                        <p>{nameUpfr}</p>
                    </>
                }
            </div>
        </div>
    )
}

