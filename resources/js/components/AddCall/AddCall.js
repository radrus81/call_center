import React, { useEffect } from "react";
import { Formik, Field } from "formik";
import { Form, SubmitBtn, Checkbox } from "react-formik-ui";
import { Label, FormGroup, Col, Alert } from "reactstrap";
import "./AddCall.scss";
import { FullNamePensioner } from "./Fields/FullNamePensioner";
import { Snils } from "./Fields/Snils";
import { Phone } from "./Fields/Phone";
import { SelectComponent } from "./Fields/SelectComponent";
import { Topics } from "./Fields/Topics";
import { SubTopics } from "./Fields/SubTopics";
import { TextArea } from "./Fields/TextArea";
import { Сomplaint } from "./Fields/Сomplaint";
import { ActionStatus } from "./Fields/ActionStatus";
import CallFormSchema from "./CallFormSchema";
import { UsersStatistic } from "./Fields/UsersStatistic";
import { UsersLastCalls } from "./Fields/UsersLastCalls";
import ModalServerError from "../../containers/ModalServerError";
import ModalHistoryCalls from "../../containers/ModalHistoryCalls";

const AddCall = ({
    access,
    isRequestSecretWord,
    message,
    stateFormik,
    areaOfDislocation,
    counterparties,
    topics,
    subTopics,
    typeConsultation,
    resultConsultation,
    upfr,
    userStatistic,
    usersCalls,
    isShowModalHistoryCall,
    addValueField,
    addCall,
    getUserCallsAndStatistic,
    ChangeStatusCall,
    getSecretWord,
    dataSecretWord,
    showModalHistoryCalls,
}) => {
    useEffect(() => {
        getUserCallsAndStatistic();
    }, []);

    return (
        <div className="wrap addCall">
            <FormGroup row>
                <Col sm={7}>
                    <h2>Добавить звонок</h2>
                    <hr />
                    <Formik
                        initialValues={stateFormik}
                        enableReinitialize={true}
                        validationSchema={CallFormSchema}
                        onSubmit={async (values, { setSubmitting }) => {
                            addCall(values);
                            setSubmitting(false);
                        }}
                    >
                        {({ errors, touched, values, isSubmitting }) => (
                            <Form>
                                <FullNamePensioner
                                    errors={errors}
                                    touched={touched}
                                    fullNamePensioner={values.fullNamePensioner}
                                    addValueField={addValueField}
                                    showModalHistoryCalls={
                                        showModalHistoryCalls
                                    }
                                />
                                <Snils
                                    access={access}
                                    isRequestSecretWord={isRequestSecretWord}
                                    dataSecretWord={dataSecretWord}
                                    errors={errors}
                                    snils={values.snils}
                                    addValueField={addValueField}
                                    getSecretWord={getSecretWord}
                                />
                                <Phone
                                    phone={values.phone}
                                    addValueField={addValueField}
                                />
                                <SelectComponent
                                    error={errors.areaOfDislocation}
                                    touched={touched.areaOfDislocation}
                                    nameComponent="areaOfDislocation"
                                    labelName="* Район проживания"
                                    data={areaOfDislocation}
                                    addValueField={addValueField}
                                />
                                <SelectComponent
                                    error={errors.typeCounterparty}
                                    touched={touched.typeCounterparty}
                                    nameComponent="typeCounterparty"
                                    labelName="* Тип контрагента"
                                    data={counterparties}
                                    addValueField={addValueField}
                                />
                                <Topics
                                    errors={errors}
                                    touched={touched}
                                    data={topics}
                                    addValueField={addValueField}
                                />
                                <SubTopics
                                    errors={errors}
                                    touched={touched}
                                    idTopic={values.topic}
                                    data={subTopics}
                                    addValueField={addValueField}
                                />
                                <SelectComponent
                                    error={errors.typeConsultation}
                                    touched={touched.typeConsultation}
                                    nameComponent="typeConsultation"
                                    labelName="* Вид консультации"
                                    data={typeConsultation}
                                    addValueField={addValueField}
                                />
                                <SelectComponent
                                    error={errors.resultConsultation}
                                    touched={touched.resultConsultation}
                                    nameComponent="resultConsultation"
                                    labelName="* Результат консультирования"
                                    data={resultConsultation}
                                    addValueField={addValueField}
                                />
                                <TextArea
                                    nameComponent="notice"
                                    labelName="Примечание"
                                    placeholder="Примечание"
                                    values={values.notice}
                                    addValueField={addValueField}
                                />
                                {access < 3 ? (
                                    <div className="inputAddCall">
                                        <FormGroup row>
                                            <Label for="exampleEmail" sm={3}>
                                                Есть жалоба на УПФР
                                            </Label>
                                            <Col sm={9}>
                                                <Checkbox
                                                    name="isСomplaint"
                                                    onClick={() => {
                                                        addValueField(
                                                            "isСomplaint",
                                                            !values.isСomplaint
                                                        );
                                                    }}
                                                    value={
                                                        values.isСomplaint ||
                                                        false
                                                    }
                                                />
                                            </Col>
                                        </FormGroup>
                                    </div>
                                ) : null}
                                {values.isСomplaint ? (
                                    <Сomplaint
                                        upfr={upfr}
                                        values={values}
                                        addValueField={addValueField}
                                    />
                                ) : null}
                                <ActionStatus addValueField={addValueField} />
                                <div className="btnAddCall">
                                    <Label sm={9} className="labelCountCalls">
                                        Количество звонков
                                    </Label>
                                    <Field
                                        name="countRecord"
                                        type="text"
                                        className={
                                            errors.countRecord
                                                ? "form-control errorValidation countRecord"
                                                : "form-control countRecord"
                                        }
                                        onBlur={event =>
                                            addValueField(
                                                "countRecord",
                                                event.target.value
                                            )
                                        }
                                        value={values.countRecord || ""}
                                    />
                                    <SubmitBtn
                                        id="send"
                                        className="btn-success"
                                        type="submit"
                                        disabled={isSubmitting}
                                    >
                                        Сохранить
                                    </SubmitBtn>
                                </div>
                                {message ? (
                                    <Alert color="success">{message}</Alert>
                                ) : null}
                            </Form>
                        )}
                    </Formik>
                </Col>
                <Col sm={5}>
                    <UsersStatistic userStatistic={userStatistic} />
                    <UsersLastCalls
                        usersCalls={usersCalls}
                        ChangeStatusCall={ChangeStatusCall}
                    />
                </Col>
            </FormGroup>
            <ModalServerError />
            <ModalHistoryCalls />
        </div>
    );
};

export default AddCall;
