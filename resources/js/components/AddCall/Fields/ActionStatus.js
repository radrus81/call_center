import React from 'react'
import { FormGroup, Col, Label } from 'reactstrap'
import { Radio } from 'react-formik-ui'


export const ActionStatus = ({ addValueField }) => {

    return (
        <>
            <div className="inputAddCall">
                <FormGroup row>
                    <Label for="exampleEmail" sm={3}>Результат общения</Label>
                    <Col sm={9} className="wrapStatus">
                        <Radio
                            name='status'
                            inline
                            className="form-status"
                            onClick={(event) => { addValueField('status', parseInt(event.target.value)) }}
                            options={[
                                { value: 0, label: 'Не обработан' },
                                { value: 1, label: 'Обработан' },
                                { value: 2, label: 'Переключен на специалиста' }
                            ]}
                        />
                        <Radio
                            name='status'
                            inline
                            className="form-status"
                            onClick={(event) => { addValueField('status', parseInt(event.target.value)) }}
                            options={[
                                { value: 3, label: 'Записан на прием' },
                                { value: 4, label: 'Записан на выдачу документов' }
                            ]}
                        />
                    </Col>
                </FormGroup>
            </div>
        </>
    )
}
