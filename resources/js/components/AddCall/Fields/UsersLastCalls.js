import React from 'react'
import { FormGroup, Col, Table } from 'reactstrap'
import { IconsStatus } from '../../Journal/IconsStatus'


const statusStyle = ['table-danger', 'table-success', 'table-primary', 'table-secondary', 'table-warning']

export const UsersLastCalls = ({ usersCalls, ChangeStatusCall }) => {

    return (
        <FormGroup row className="usersStatistic">
            <Col sm={12}>
                <h2>Ваши последние принятые звонки</h2>
                <hr />
                <Table bordered >
                    <thead className="thead-dark">
                        <tr>
                            <th>Дата звонка</th>
                            <th>Ф.И.О</th>
                            <th>Тема обращения</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            usersCalls.map((call, index) => {
                                return (
                                    <tr key={call.idCall} className={statusStyle[call.status]}>
                                        <td>{call.dateStart}</td>
                                        <td>{call.fullNamePensioner}</td>
                                        <td>{call.subtopic}</td>
                                        <td><IconsStatus
                                            idObjectCall={index}
                                            idCall={call.idCall}
                                            idStatus={call.status}
                                            ChangeStatusCall={ChangeStatusCall}
                                        /></td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </Table>
            </Col>
        </FormGroup>
    )
}
