import React from 'react'
import { Field } from 'formik'
import { FormGroup, Col, Label } from 'reactstrap'


export const TextArea = ({ nameComponent, placeholder, labelName, values, addValueField }) => {
    return (
        <>
            <div className="inputAddCall">
                <FormGroup row>
                    <Label for="exampleEmail" sm={3}>{labelName}</Label>
                    <Col sm={9}>
                        <Field
                            name={nameComponent}
                            placeholder={placeholder}
                            component="textarea"
                            className="form-control"
                            value={values || ''}
                            onBlur={event =>
                                addValueField(nameComponent, event.target.value)
                            }
                        />
                    </Col>
                </FormGroup>
            </div>
        </>
    )
}
