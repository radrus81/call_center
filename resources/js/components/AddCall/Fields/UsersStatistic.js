import React from 'react'
import { FormGroup, Col, Table } from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheckCircle, faUserPlus, faFile, faComments, faStopCircle } from '@fortawesome/free-solid-svg-icons'


export const UsersStatistic = ({ userStatistic }) => {

    return (
        <FormGroup row className="usersStatistic">
            <Col sm={12}>
                <h2>Статистика за день</h2>
                <hr />
                <Table bordered >
                    <thead>
                        <tr>
                            <th><FontAwesomeIcon title="Не обработан" icon={faStopCircle} className="text-danger" size="lg" /></th>
                            <th><FontAwesomeIcon title="Обработан" icon={faCheckCircle} className="text-success" size="lg" /></th>
                            <th><FontAwesomeIcon title="Переключен на специалиста" icon={faUserPlus} className="text-primary" size="lg" /></th>
                            <th><FontAwesomeIcon title="Записан на прием" icon={faComments} className="text-secondary" size="lg" /></th>
                            <th><FontAwesomeIcon title="Записан на выдачу документов" icon={faFile} className="text-warning" size="lg" /></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            {userStatistic.map((status, index) => {
                                return (
                                    <td key={index}>{status.count}</td>
                                )
                            })
                            }
                        </tr>
                    </tbody>
                </Table>
            </Col>
        </FormGroup>
    )
}
