import React from "react";
import { Field } from "formik";
import { FormGroup, Col, Label } from "reactstrap";
import MaskedInput from "react-text-mask";
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faClipboard } from '@fortawesome/free-solid-svg-icons'

const phoneNumberMask = [
    "+",
    "7",
    "(",
    /[0-9]/,
    /\d/,
    /\d/,
    ")",
    " ",
    /\d/,
    /\d/,
    /\d/,
    "-",
    /\d/,
    /\d/,
    /\d/,
    /\d/
];

export const Phone = ({ phone, addValueField }) => {
    return (
        <>
            <div className="inputAddCall">
                <FormGroup row>
                    <Label for="exampleEmail" sm={3}>
                        Номер телефона
                    </Label>
                    <div className="colClipboard">
                        <CopyToClipboard text={phone}
                        >
                            <FontAwesomeIcon title="Скопировать в буфер" icon={faClipboard} className="text-info clipboard" size="lg" />
                        </CopyToClipboard>
                    </div>
                    <Col sm={8}>
                        <Field
                            name="phone"
                            render={({ field }) => (
                                <MaskedInput
                                    {...field}
                                    mask={phoneNumberMask}
                                    placeholder="Введите номер телефона"
                                    type="text"
                                    className="form-control"
                                    value={phone || ""}
                                    onBlur={event =>
                                        addValueField(
                                            "phone",
                                            event.target.value
                                        )
                                    }
                                />
                            )}
                        />
                    </Col>
                </FormGroup>
            </div>
        </>
    );
};
