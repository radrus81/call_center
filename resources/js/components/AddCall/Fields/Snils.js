import React, { useState } from "react";
import { Field } from "formik";
import { FormGroup, Col, Label, Button } from "reactstrap";
import { formatValueSnils } from "../../Helpers/Helpers";
import { TableSecretWord } from "./TableSecredWord";
import MaskedInput from "react-text-mask";
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faClipboard } from '@fortawesome/free-solid-svg-icons'


const snilsMask = [
    /[0-9]/,
    /\d/,
    /\d/,
    "-",
    /\d/,
    /\d/,
    /\d/,
    "-",
    /\d/,
    /\d/,
    /\d/,
    " ",
    /\d/,
    /\d/
];




export const Snils = ({
    access,
    isRequestSecretWord,
    dataSecretWord,
    errors,
    snils,
    addValueField,
    getSecretWord
}) => {
    return (
        <>
            <div className="inputAddCall">
                <FormGroup row>
                    <Label for="exampleEmail" sm={3}>
                        СНИЛС
                    </Label>
                    <div className="colClipboard">
                        <CopyToClipboard text={snils}
                        >
                            <FontAwesomeIcon title="Скопировать в буфер" icon={faClipboard} className="text-info clipboard" size="lg" />
                        </CopyToClipboard>
                    </div>
                    <Col sm={5}>
                        <Field
                            name="snils"
                            render={({ field }) => (
                                <MaskedInput
                                    {...field}
                                    mask={snilsMask}
                                    placeholder="Введите СНИЛС"
                                    type="text"
                                    className={
                                        errors.snils
                                            ? "form-control errorValidation"
                                            : "form-control"
                                    }
                                    value={snils || ""}
                                    onBlur={event =>
                                        addValueField(
                                            "snils",
                                            formatValueSnils(event.target.value)
                                        )
                                    }
                                />
                            )}
                        />
                    </Col>

                    <Col sm={3}>
                        <Button
                            color="success"
                            disabled={snils ? false : true}
                            onClick={() => getSecretWord()}
                        >
                            Секретное слово
                        </Button>
                    </Col>
                    {isRequestSecretWord ? (
                        <div className="identityClient" sm={12}>
                            <Label sm={3}>Идентификация</Label>
                            <Col sm={9} className="tableCol">
                                <TableSecretWord
                                    access={access}
                                    dataSecretWord={dataSecretWord}
                                    addValueField={addValueField}
                                />
                            </Col>
                        </div>
                    ) : null}
                </FormGroup>
            </div>
        </>
    );
};
