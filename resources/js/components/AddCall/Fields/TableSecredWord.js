import React from "react";
import { Table } from "reactstrap";
import { Radio } from "react-formik-ui";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { ACCESS_USER_UPFR } from "../../../enum/enum";

export const TableSecretWord = ({ access, dataSecretWord, addValueField }) => {
    return (
        <>
            <Table bordered className="tableSecretWord">
                <thead className="thead-dark">
                    <tr>
                        <th>База</th>
                        <th>Дата регист.</th>
                        <th>Контрольный вопрос</th>
                        <th>Секретное слово</th>
                    </tr>
                </thead>
                <tbody>
                    {!dataSecretWord.length ? (
                        <tr>
                            <td colSpan="4">
                                <FontAwesomeIcon
                                    className="mr-2 fa-spin ml-3 fa-lg"
                                    icon={faSpinner}
                                />
                                Ждите идет запрос...
                            </td>
                        </tr>
                    ) : (
                        dataSecretWord.map((data, index) => {
                            return (
                                <tr key={index}>
                                    <td>{data.base}</td>
                                    <td>{data.createDate}</td>
                                    <td>{data.question}</td>
                                    <td>{data.answer}</td>
                                </tr>
                            );
                        })
                    )}
                </tbody>
            </Table>
            {access !== ACCESS_USER_UPFR ? (
                <Radio
                    name="codeWord"
                    inline
                    className="form-status"
                    onClick={event => {
                        addValueField("codeWord", parseInt(event.target.value));
                    }}
                    options={[
                        { value: 0, label: "Не спрашивалось" },
                        { value: 1, label: "Ответил правильно" },
                        { value: 2, label: "Ответил неправильно" }
                    ]}
                />
            ) : null}
        </>
    );
};
