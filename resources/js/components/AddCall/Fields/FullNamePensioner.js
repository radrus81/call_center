import React from 'react'
import { Field } from 'formik'
import { Button } from 'react-formik-ui'
import { FormGroup, Col, Label } from 'reactstrap'
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faClipboard } from '@fortawesome/free-solid-svg-icons'


export const FullNamePensioner = ({ errors, touched, fullNamePensioner, addValueField, showModalHistoryCalls }) => {

    return (
        <>
            < div className="inputAddCall" >
                <FormGroup row>
                    <Label for="exampleEmail" sm={3}> Ф.И.О</Label>
                    <div className="colClipboard">
                        <CopyToClipboard text={fullNamePensioner}
                        >
                            <FontAwesomeIcon title="Скопировать в буфер" icon={faClipboard} className="text-info clipboard" size="lg" />
                        </CopyToClipboard>
                    </div>
                    <Col sm={7}>
                        <Field
                            name="fullNamePensioner"
                            placeholder="Введите фамилию имя отчество через пробел"
                            type="text"
                            className="form-control"
                            /*className={errors.fullNamePensioner && touched.fullNamePensioner ?
                                "form-control errorValidation"
                                : "form-control"
                            }*/
                            value={fullNamePensioner || ''}
                            onBlur={event =>
                                addValueField('fullNamePensioner', event.target.value)
                            }
                        />
                    </Col>
                    <div>
                        <Button className="btn-success"
                            onClick={() => showModalHistoryCalls()}>История</Button>
                    </div>
                </FormGroup>
            </div >
        </>
    )
}
