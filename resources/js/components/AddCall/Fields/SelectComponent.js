import React from 'react'
import { Field } from 'formik'
import { FormGroup, Col, Label } from 'reactstrap'


export const SelectComponent = ({ error, touched, nameComponent, labelName, data, addValueField }) => {

    return (
        <>
            <div className="inputAddCall">
                <FormGroup row>
                    <Label for="exampleEmail" sm={3}>{labelName}</Label>
                    <Col sm={9}>
                        <Field
                            component="select"
                            name={nameComponent}
                            className={error && touched ?
                                "form-control errorValidation"
                                : "form-control"
                            }
                            onChange={event =>
                                addValueField(nameComponent, event.target.value)
                            }>
                            <option value="0">---</option>
                            {
                                data.map((option, index) => {
                                    return (
                                        <option
                                            key={index}
                                            value={option.id}>
                                            {option.name}
                                        </option>
                                    )
                                })
                            }
                        </Field>
                    </Col>
                </FormGroup>
            </div>
        </>
    )
}
