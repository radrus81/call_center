import * as Yup from "yup";

const CallFormSchema = Yup.object().shape({
    /* fullNamePensioner: Yup.string()
         //.required("Не заполнено поле")
         .matches(/^(?! )(?!.* $)((?:.* ){2}).+$/, 'Неверное ФИО. Должен быть: Ф И О'),*/
    snils: Yup.string()
        .min(14, "Должно быть не менее 14 символов"),
    areaOfDislocation: Yup.string().matches(/^(?!0*$)\d*$/, 'Не выбран район'),
    typeCounterparty: Yup.string().matches(/^(?!0*$)\d*$/, 'Не выбран район'),
    topic: Yup.string().matches(/^(?!0*$)\d*$/, 'Не выбран район'),
    subTopic: Yup.string().matches(/^(?!0*$)\d*$/, 'Не выбран район'),
    typeConsultation: Yup.string().matches(/^(?!0*$)\d*$/, 'Не выбран район'),
    resultConsultation: Yup.string().matches(/^(?!0*$)\d*$/, 'Не выбран район'),
    countRecord: Yup.string().matches(/^(?!0*$)\d*$/, 'Не число')
});

export default CallFormSchema
