import React from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Table } from 'reactstrap'
import { getTitle } from './TitleColumns'
import { Tablerow } from './Tablerow'
import './Modaldetailcall.scss'
import { ACCESS_ADMIN, ACCESS_USER_CONTROLLER_OPFR } from '../../../enum/enum'



export const Modaldetailcall = ({ isModalOpen, tableCallsCellkey,
    isModalClose, dataCallInObject, showEditField, modalClose,
    lossOfFocus, saveEditedCall, fioUser, access }) => {

    const dataCallInArray = []

    for (var key in dataCallInObject) {
        let title = getTitle(key)
        if (title) {
            dataCallInArray.push({
                key,
                header: title.header,
                value: dataCallInObject[key],
                isEdit: title.isEdit
            })
        }
    }

    const lossOfFocusInfield = event => {
        if (event.key === 'Enter') {
            lossOfFocus()
        }
    }

    const closeBtn = <button className="close" onClick={modalClose}>&times;</button>;
    return (

        < Modal size="lg" isOpen={isModalOpen}
            unmountOnClose={isModalClose}
            backdropClassName="modalsFon"

        >
            <ModalHeader close={closeBtn}>Пенсионер: {dataCallInObject.familyClient + ' '
                + dataCallInObject.nameClient + ' ' + dataCallInObject.middleNameClient}<br />
            </ModalHeader>
            <ModalBody>
                <Table className="infoAboutPensioner">
                    <tbody>
                        <Tablerow
                            tableCallsCellkey={tableCallsCellkey}
                            dataCallInArray={dataCallInArray}
                            showEditField={showEditField}
                            lossOfFocusInfield={lossOfFocusInfield}
                        />
                    </tbody>
                </Table>
            </ModalBody>
            <ModalFooter>
                {
                    access === ACCESS_ADMIN || access === ACCESS_USER_CONTROLLER_OPFR
                        || dataCallInObject['nameUser'] === fioUser ?
                        <Button color="success" onClick={saveEditedCall}>Сохранить</Button>
                        : null
                }
                <Button color="danger" onClick={modalClose}>Закрыть</Button>
            </ModalFooter>
        </Modal >
    )
}
