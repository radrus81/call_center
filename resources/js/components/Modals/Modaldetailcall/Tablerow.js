import React from 'react'
import ComponentEditField from './ComponentsField/ComponentIndex'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit, faBan } from '@fortawesome/free-solid-svg-icons'

export const Tablerow = ({ tableCallsCellkey, dataCallInArray, showEditField, lossOfFocusInfield }) => {

    return (
        dataCallInArray.map((field, index) => {
            var FieldForEdit = ComponentEditField[field.key]
            return (
                <tr key={index}>
                    <th>{field.header}</th>
                    {
                        field.isEdit ?
                            <td className="infoIcon" onClick={() => (showEditField(field.key))}>
                                {!tableCallsCellkey ?
                                    !localStorage.getItem(field.key) ? field.value : localStorage.getItem(field.key)
                                    : tableCallsCellkey == field.key ?
                                        <FieldForEdit
                                            value={!localStorage.getItem(field.key) ? field.value : localStorage.getItem(field.key)}
                                            fieldKey={field.key}
                                            lossOfFocusInfield={lossOfFocusInfield}
                                        />
                                        : !localStorage.getItem(field.key) ? field.value : localStorage.getItem(field.key)
                                }
                                &nbsp;<FontAwesomeIcon className="text-success" icon={faEdit} size="lg" />
                            </td>
                            : <td className="infoIcon">{field.value}
                                &nbsp;<FontAwesomeIcon className="text-danger" icon={faBan} size="lg" />
                            </td>
                    }
                </tr>
            )
        })
    )
}
