import React from 'react'

export const DropDownListField = ({ fieldKey, value,
    areaOfDislocation, counterparties, oldTopic, topics,
    typeConsultation, resultConsultation, upfr, lossOfFocusInfield }) => {

    var componentField;
    switch (fieldKey) {
        case 'areaOfDislocation': componentField = areaOfDislocation; break
        case 'typeOfCounterparty': componentField = counterparties; break
        case 'nameTopicOld': componentField = oldTopic; break
        case 'topic': componentField = topics; break
        case 'typeConsultation': componentField = typeConsultation; break
        case 'resultConsultation': componentField = resultConsultation; break
        case 'complaintСodeUpfr': componentField = upfr; break
        default: componentField = []
    }
    return (

        <select
            autoFocus
            className="form-control"
            onChange={event => localStorage.setItem(fieldKey, event.target.value)}
            onKeyPress={lossOfFocusInfield}
        >

            <option selected="selected"></option>
            {
                componentField.map((option, index) => {
                    return (
                        <option
                            key={option.id + index}
                            defaultValue={option.name}
                        >
                            {option.name}
                        </option>
                    )
                })
            }
        </select>
    )
}

