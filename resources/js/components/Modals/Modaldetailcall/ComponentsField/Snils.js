import React from "react";
import { formatValueSnils } from "../../../Helpers/Helpers";
import MaskedInput from "react-text-mask";

const Snils = ({ value, lossOfFocusInfield }) => (
    <MaskedInput
        type="text"
        mask={[
            /[0-9]/,
            /\d/,
            /\d/,
            "-",
            /\d/,
            /\d/,
            /\d/,
            "-",
            /\d/,
            /\d/,
            /\d/,
            " ",
            /\d/,
            /\d/
        ]}
        autoFocus
        placeholder="Введите СНИЛС"
        className="form-control"
        defaultValue={value}
        onChange={event =>
            localStorage.setItem("snils", formatValueSnils(event.target.value))
        }
        onKeyPress={lossOfFocusInfield}
    ></MaskedInput>
);

export default Snils;
