import React from 'react'

const FioClient = ({ fieldKey, value, lossOfFocusInfield }) => (
    <input type="text"
        autoFocus
        className="form-control"
        defaultValue={value}
        onChange={event =>
            localStorage.setItem(fieldKey, event.target.value)}
        onKeyPress={lossOfFocusInfield}
    ></input >
)

export default FioClient
