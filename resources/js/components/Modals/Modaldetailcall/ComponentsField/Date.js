import React from 'react'
import StringMask from "string-mask"


const Date = ({ fieldKey, value, lossOfFocusInfield }) => (

    <input type="text"
        autoFocus
        maxLength="10"
        className="form-control"
        defaultValue={value}
        onChange={event =>
            localStorage.setItem(fieldKey,
                formatValue(event.target.value)
            )}
        onKeyPress={lossOfFocusInfield}
    ></input >
)
var DELIMITER = "."
const MASK = "00.00.0000"

function removeTrailingCharIfFound(str, char) {
    return str
        .split(char)
        .filter(segment => segment !== char)
        .join(char);
}

function formatValue(str) {
    var unmaskedValue = str.split(DELIMITER).join("");
    const formatted = StringMask.process(unmaskedValue, MASK);
    return removeTrailingCharIfFound(formatted.result, DELIMITER);
}

export default Date
