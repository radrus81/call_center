import React from 'react'

export const Subtopic = ({ fieldKey, subTopics, topicCall, lossOfFocusInfield }) => {

    if (localStorage.getItem('topic')) {
        topicCall = localStorage.getItem('topic')
    }
    return (
        < select
            autoFocus
            className="form-control"
            onChange={event => localStorage.setItem(fieldKey, event.target.value)}
            onKeyPress={lossOfFocusInfield}
        >
            {
                subTopics.filter((props) => props.topic === topicCall).map((option, index) => {
                    return (
                        <option
                            key={option.id}
                            value={option.name}
                        >

                            {option.name}
                        </option>
                    )
                })
            }
        </select >
    )
}


