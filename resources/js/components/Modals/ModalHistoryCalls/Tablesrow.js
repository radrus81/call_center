import React from 'react'
import { IconsStatus } from '../../Journal/IconsStatus'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCopy } from '@fortawesome/free-solid-svg-icons'


const statusStyle = ['table-danger', 'table-success', 'table-primary', 'table-secondary', 'table-warning']

export const Tablesrow = ({ calls, copyCall }) => {

    return (
        <>
            {
                !calls.length ?
                    <tr className={statusStyle[0]}>
                        <td colSpan="10"><div className="alert alert-danget">Ничего не найдено</div></td>
                    </tr>
                    :
                    calls.map((call, index) => {
                        return (
                            <tr key={call.id + ' ' + index} className={statusStyle[call.status]}>
                                <td><FontAwesomeIcon
                                    title="Скопировать"
                                    icon={faCopy}
                                    size="lg"
                                    className="iconFaSearch"
                                    onClick={() => { copyCall(index) }}
                                /></td>
                                <td>{call.dateStart}</td>
                                <td>{call.familyClient + ' ' + call.nameClient + ' ' + call.middleNameClient}</td>
                                <td>{call.snils}</td>
                                <td>{call.phone}</td>
                                <td>{call.areaOfDislocation}</td>
                                <td>{call.subtopic}</td>
                                <td>{call.prim}</td>
                                <td>{call.nameUser}</td>
                                <td>
                                    <IconsStatus
                                        idObjectCall={index}
                                        idCall={call.idCall}
                                        idStatus={call.status}
                                    />
                                </td>
                            </tr>)
                    })
            }
        </>
    )
}
