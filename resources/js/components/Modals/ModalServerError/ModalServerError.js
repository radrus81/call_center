import React from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'

export const ModalServerError = ({ isModalOpen, isModalClose, codeError, modalClose }) => {

    return (
        < Modal size="lg" isOpen={isModalOpen} unmountOnClose={isModalClose}>
            <ModalHeader>Ошибка сервера!<br /></ModalHeader>
            <ModalBody>
                <div className="alert alert-danger" role="alert">
                    Произошла ошибка на сервере. Код ошибки {codeError}<br />
                    Повторите попытку еще раз.<br />
                    При повторном появлении сообщения об ошибке, обратитесь к администратору.
                </div>
            </ModalBody>
            <ModalFooter>
                <Button color="secondary" onClick={modalClose}>OK</Button>
            </ModalFooter>
        </Modal >
    )
}
