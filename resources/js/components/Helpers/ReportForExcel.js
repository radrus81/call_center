
export function createReportExcel(dataForReport, isSelectedReportUpfr) {
    const columnLetter = ['B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']
    var statistic = []
    var totalNotifi = dataForReport.length
    var countOut = 0
    var lastRow = new Object()
    var offsetRow = 3
    var isTypeNotifiSms = false

    dataForReport.map((dataReport, index) => {

        var stat = new Object()

        var countColumnLetter = 0
        var startColNameForFormula = 'B'
        if (isSelectedReportUpfr) {
            countColumnLetter = 1
            startColNameForFormula = 'C'
        }

        dataReport.map((data, index1) => {
            if (isSelectedReportUpfr) {
                if (stat.nameUpfr == undefined) { stat.nameUpfr = data.nameUpfr }
                if (stat.nameUpfr == data.nameUpfr) {
                    stat.nameUpfr = data.nameUpfr
                } else {
                    statistic.push(stat)
                    stat = new Object()
                }
            }
            stat.notifiName = data.notifiName
            lastRow.notifiName = 'Всего'
            if (data.nameCount == 'countCorrect' || data.nameCount == 'countFix') {
                countOut += data.count
                isTypeNotifiSms = true
            } else {
                stat[data.nameCount] = data.count
                lastRow[data.nameCount] = { formula: "SUM(" + columnLetter[countColumnLetter] + "4:" + columnLetter[countColumnLetter] + (totalNotifi + offsetRow) + ")" }
                countColumnLetter++
            }
        })
        let totFormula = "SUM(" + startColNameForFormula + (index + 4) + ":" + columnLetter[countColumnLetter - 1] + (index + 4) + ")";
        stat['total'] = { formula: totFormula };
        lastRow['total'] = { formula: "SUM(" + columnLetter[countColumnLetter] + "4:" + columnLetter[countColumnLetter] + (totalNotifi + offsetRow) + ")" }
        if (isTypeNotifiSms) { stat.countOut = countOut }
        statistic.push(stat)
    })
    if (!isSelectedReportUpfr) {
        statistic.push(lastRow)
    }
    return statistic
}

