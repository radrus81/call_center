import XLSX from 'exceljs'
import saveAs from 'save-as'
import { convertObjectToArray } from './Helpers'
import { getTitle } from '../../components/Modals/Modaldetailcall/TitleColumns'



export function createExcel(dataForExcel) {
    
    let startRow = 3
    let wb = new XLSX.Workbook();
    let fileName = dataForExcel[0]
    for (var i = 1; i < dataForExcel.length; i++) {
        let ws = wb.addWorksheet(dataForExcel[i].worksheetName)
        var column = createHeader(dataForExcel[i].data)
        var arrKeyForTitle = []
        column.map((tr, index) => {
            arrKeyForTitle.push(tr.header)
        })
        ws.getRow(startRow).values = arrKeyForTitle
        ws.columns = column
        ws.getRow(startRow).font = { bold: true }
        ws.getRow(startRow).alignment = { wrapText: true, horizontal: "center" }
        ws.addRows(dataForExcel[i].data)
        console.log(dataForExcel[i].data)
        var wsAndLastAddressCell = setBorders(ws, dataForExcel[i].data, startRow)
        ws = wsAndLastAddressCell.ws
        ws.mergeCells('A1', wsAndLastAddressCell.lastAddressCell + '1')
        ws.getCell('A1').value = dataForExcel[i].title
    }
    saveXlsx(wb, fileName)
}



export function createExcelForReportSubjectOfRequests(dataForExcel) {

    var mainData = dataForExcel[1]['data']

    var nameTopic = mainData['nameTopic']
    var nameSubTopic = mainData['nameSubTopic']


    let startRow = 3
    let wb = new XLSX.Workbook();
    let fileName = dataForExcel[0]
    let ws = wb.addWorksheet(dataForExcel[0].worksheetName)

    var col2 = ws.getColumn('B');
    var col3 = ws.getColumn('C');
    col2.width = 60
    col3.width = 35

    ws.mergeCells('A1', 'C1')
    ws.getCell('A1').value = dataForExcel[1].title
    ws.getCell('A1').alignment = { vertical: 'middle', horizontal: 'center' };
    ws.getCell('A1').font = { bold: true };

    ws.mergeCells('A3', 'B4')
    ws.getCell('A3').value = 'Наименование показателя'
    ws.getCell('A3').alignment = { vertical: 'middle', horizontal: 'center' };
    ws = setCellOnly(ws, 'A3')
    ws.getCell('A3').font = { bold: true }

    ws.getCell('C3').value = 'Количественный показатель'
    ws.getCell('C3').alignment = { vertical: 'middle', horizontal: 'center' };
    ws.getCell('C3').font = { bold: true };
    ws.getCell('C4').value = '1'
    ws = setCellOnly(ws, 'C3')
    ws = setCellOnly(ws, 'C4')
    ws.getCell('C4').alignment = { horizontal: 'center' }

    ws.mergeCells('A5', 'B5')
    ws.getCell('A5').value = 'Количество контакт центров'
    ws = setCellOnly(ws, 'A5')
    ws = setCellOnly(ws, 'C5')

    ws.mergeCells('A6', 'A7')
    ws.getCell('A6').value = 'из них:'
    ws.getCell('A6').alignment = { vertical: 'middle', horizontal: 'center' };
    ws = setCellOnly(ws, 'A6')
    ws = setCellOnly(ws, 'C6')

    ws.getCell('B6').value = 'регионального уровня'
    ws.getCell('B7').value = 'городского уровня'
    ws.getCell('B6').alignment = { horizontal: 'right' }
    ws.getCell('B7').alignment = { horizontal: 'right' }
    ws = setCellOnly(ws, 'B7')
    ws = setCellOnly(ws, 'C7')

    ws.mergeCells('A8', 'B8')
    ws.getCell('A8').value = 'Количество обращений в контакт-центры'
    ws = setCellOnly(ws, 'A8')
    ws = setCellOnly(ws, 'C8')

    ws.mergeCells('A9', 'A10')
    ws.getCell('A10').value = 'из них:'
    ws = setCellOnly(ws, 'A9')
    ws.getCell('A9').alignment = { vertical: 'middle', horizontal: 'center' };
    ws = setCellOnly(ws, 'C9')

    ws.getCell('B9').value = 'регионального уровня'
    ws.getCell('C9').value = mainData.totalCalls
    ws.getCell('C9').alignment = { horizontal: 'center' }
    ws.getCell('B10').value = 'городского уровня'
    ws.getCell('B9').alignment = { horizontal: 'right' }
    ws.getCell('B10').alignment = { horizontal: 'right' }
    ws = setCellOnly(ws, 'B9')
    ws = setCellOnly(ws, 'B10')
    ws = setCellOnly(ws, 'C10')

    ws.mergeCells('A11', 'B11')
    ws.getCell('A11').value = 'Количество специалистов контакт-центра, всего'
    ws = setCellOnly(ws, 'C11')

    ws.mergeCells('A12', 'A13')
    ws.getCell('A12').value = 'из них:'
    ws = setCellOnly(ws, 'A12')
    ws.getCell('A12').alignment = { vertical: 'middle', horizontal: 'center' }
    ws = setCellOnly(ws, 'C12')

    ws.getCell('B12').value = 'регионального уровня'
    ws.getCell('B13').value = 'городского уровня'
    ws.getCell('B12').alignment = { horizontal: 'right' }
    ws.getCell('B13').alignment = { horizontal: 'right' }
    ws = setCellOnly(ws, 'B12')
    ws = setCellOnly(ws, 'B13')
    ws = setCellOnly(ws, 'C13')

    ws.mergeCells('A14', 'B14')
    ws.getCell('A14').value = 'Количество телефонов горячих линий'
    ws = setCellOnly(ws, 'A14')
    ws = setCellOnly(ws, 'C14')

    ws.mergeCells('A15', 'B15')
    ws.getCell('A15').value = 'Количество обращений на телефоны "горячих линий"'
    ws.getCell('C15').value = mainData.totalCallsHotLine
    ws = setCellOnly(ws, 'A15')
    ws = setCellOnly(ws, 'C15')
    ws.getCell('C15').alignment = { horizontal: 'center' }

    //темы обращения
    ws.mergeCells('A16', 'B16')
    ws.getCell('A16').value = 'Темы обращений'
    ws = setCellOnly(ws, 'A16')
    ws = setCellOnly(ws, 'C16')
    var currentRow = 17;
    for (var idTopic in mainData) {
        if (!isNaN(idTopic)) {
            if (idTopic < 10) { var idTopicText = `0${idTopic}(${nameTopic[idTopic]})` }
            else { var idTopicText = `${idTopic}(${nameTopic[idTopic]})` }
            ws.getCell('B' + currentRow).value = idTopicText;
            ws.getCell('B' + currentRow).font = { bold: true };
            ws = setCellOnly(ws, 'B' + currentRow)
            ws = setCellOnly(ws, 'C' + currentRow)
            currentRow++;
            for (var codeSubtopic in mainData[idTopic]) {
                ws.getCell('B' + currentRow).value = `${codeSubtopic}.(${nameSubTopic[idTopic][codeSubtopic]})`
                ws.getCell('B' + currentRow).alignment = { horizontal: 'right' }
                ws = setCellOnly(ws, 'B' + currentRow)
                ws.getCell('C' + currentRow).value = mainData[idTopic][codeSubtopic]
                ws.getCell('C' + currentRow).alignment = { horizontal: 'center' };
                ws = setCellOnly(ws, 'C' + currentRow)
                currentRow++;
            }
        } else {
            break;
        }
    }
    ws.mergeCells('A17', 'A' + (currentRow - 1))
    ws.getCell('A17').value = 'из них:'
    ws = setCellOnly(ws, 'A17')
    ws = setCellOnly(ws, 'C17')
    ws.getCell('A17').alignment = { vertical: 'middle', horizontal: 'center' };
    //Вид консультации
    ws.mergeCells('A' + currentRow, 'B' + currentRow)
    ws.getCell('A' + currentRow).value = 'Вид консультации'
    ws = setCellOnly(ws, 'A' + currentRow)
    currentRow++;
    var memoryCurrentRow = currentRow;
    for (var idVid in mainData['typeConsult']) {
        ws.getCell('B' + currentRow).value = `0${idVid}(${mainData['nameConsult'][idVid]})`;
        ws = setCellOnly(ws, 'B' + currentRow)
        ws.getCell('B' + currentRow).alignment = { horizontal: 'right' };
        ws.getCell('C' + currentRow).value = mainData['typeConsult'][idVid];
        ws.getCell('C' + currentRow).alignment = { horizontal: 'center' };
        ws = setCellOnly(ws, 'C' + currentRow)
        currentRow++;
    }
    ws.mergeCells('A' + memoryCurrentRow, 'A' + (currentRow - 1))
    ws.getCell('A' + memoryCurrentRow).value = 'из них:'
    //Результат консультирования (информирования)
    ws.mergeCells('A' + currentRow, 'B' + currentRow)
    ws.getCell('A' + currentRow).value = 'Результат консультирования (информирования)'
    ws = setCellOnly(ws, 'A' + currentRow)
    currentRow++;
    var memoryCurrentRow = currentRow;
    for (var idResConsult in mainData['resultConsultation']) {
        ws.getCell('B' + currentRow).value = `0${idResConsult} (${mainData['nameResultConsultation'][idResConsult]})`;
        ws = setCellOnly(ws, 'B' + currentRow)
        ws.getCell('B' + currentRow).alignment = { horizontal: 'right' };
        ws.getCell('C' + currentRow).value = mainData['resultConsultation'][idResConsult];
        ws.getCell('C' + currentRow).alignment = { horizontal: 'center' };
        ws = setCellOnly(ws, 'C' + currentRow)
        currentRow++;
    }
    ws.mergeCells('A' + memoryCurrentRow, 'A' + (currentRow - 1))
    ws.getCell('A' + memoryCurrentRow).value = 'из них:'
    ws = setCellOnly(ws, 'A' + memoryCurrentRow)
    ws.getCell('A' + memoryCurrentRow).alignment = { vertical: 'middle', horizontal: 'center' };
    //продолжительность разговора
    ws.mergeCells('A' + currentRow, 'B' + currentRow)
    ws.getCell('A' + currentRow).value = 'Продолжительность разговора (мин)'
    ws = setCellOnly(ws, 'A' + currentRow)
    // ws.getCell('C' + currentRow).value = 'вычислить'
    ws = setCellOnly(ws, 'C' + currentRow)

    saveXlsx(wb, fileName)
}

function setCellOnly(ws, adressCell) {
    ws.getCell(adressCell).border = {
        top: { style: 'thin' },
        left: { style: 'thin' },
        bottom: { style: 'thin' },
        right: { style: 'thin' }
    }
    return ws
}

//формируем строки с наименованием колонки
function createHeader(data) {

    var arrData = (convertObjectToArray(data))
    var columns = []
    arrData[0].forEach(element => {
        if (getTitle(element)) {
            columns.push(getTitle(element))
        }
    });
    return columns
}

//установка границы для каждой ячейки
function setBorders(ws, data, startRow) {
    for (var indexRow = startRow - 1; indexRow <= data.length + (startRow - 1); indexRow++) {
        for (var i = 1; i <= ws.columns.length; i++) {
            var addressCell = ws.getRow(indexRow + 1).getCell(i).address
            ws.getCell(addressCell).border = {
                top: { style: 'thin' },
                left: { style: 'thin' },
                bottom: { style: 'thin' },
                right: { style: 'thin' }
            }
        }
    }
    return { 'ws': ws, 'lastAddressCell': parseLastCell(addressCell) }
}

//border для конкретной ячейки
function parseLastCell(lastCell) {
    var nameLastCell = ''
    for (var i = 0; i < lastCell.length; i++) {
        if (!parseInt(lastCell[i])) {
            nameLastCell += lastCell[i]
        }
    }
    return nameLastCell
}

//сохранение и отдаем клменту готовый файл
function saveXlsx(wb, fileName) {
    wb.xlsx.writeBuffer()
        .then(function (buffer) {
            saveAs(
                new Blob([buffer], { type: "application/octet-stream" }),
                fileName
            );
        });
}








