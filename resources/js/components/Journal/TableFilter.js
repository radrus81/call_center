import React from "react";
import { Input, Button } from "reactstrap";
import { SelectField } from "./SelectField";
import DatePicker, { registerLocale } from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import ru from "date-fns/locale/ru"; // the locale you want
registerLocale("ru", ru);

export const TableFilter = props => {
    return (
        <tr>
            <td widtd="7%">
                <Button
                    color="primary"
                    size="md"
                    onClick={() => {
                        props.startFilter(1);
                    }}
                >
                    Найти
                </Button>
            </td>
            <td widtd="7%">
                <SelectField
                    defaultValue="0"
                    defaultValueName="..."
                    fieldData={props.upfr}
                    fieldName="codeUpfr"
                    setValueForFilter={props.setValueForFilter}
                />
            </td>
            <td widtd="10%">
                <DatePicker
                    selected={props.createDateFromFilter}
                    locale="ru"
                    onChange={date =>
                        props.setValueForFilter("createDate", date)
                    }
                    placeholderText="Дата звонка"
                    dateFormat="dd.MM.yyyy"
                    className="form-control"
                />
            </td>
            <td widtd="14%">
                <Input
                    placeholder="поиск по фамилии"
                    value={props.fullNameClientFromFilter}
                    onChange={event =>
                        props.setValueForFilter(
                            "fullNameClient",
                            event.target.value
                        )
                    }
                    onKeyPress={e => {
                        e.key === "Enter" ? props.startFilter(1) : null;
                    }}
                />
            </td>
            <td widtd="24%">
                <Input
                    placeholder="поиск по тел."
                    value={props.phoneClient}
                    onChange={event =>
                        props.setValueForFilter("phone", event.target.value)
                    }
                    onKeyPress={e => {
                        e.key === "Enter" ? props.startFilter(1) : null;
                    }}
                />
            </td>
            <td widtd="10%">
                <SelectField
                    defaultValue="0"
                    defaultValueName="..."
                    fieldData={props.counterparties}
                    fieldName="typeContractor"
                    setValueForFilter={props.setValueForFilter}
                />
            </td>
            <td widtd="20%">
                <SelectField
                    defaultValue="0"
                    defaultValueName="..."
                    fieldData={props.subTopics}
                    fieldName="subTopic"
                    setValueForFilter={props.setValueForFilter}
                />
            </td>
            <td widtd="15%">
                <Input
                    placeholder="поиск по примечанию"
                    value={props.notice}
                    onChange={event =>
                        props.setValueForFilter("notice", event.target.value)
                    }
                    onKeyPress={e => {
                        e.key === "Enter" ? props.startFilter(1) : null;
                    }}
                />
            </td>
            <td widtd="9%">
                <SelectField
                    defaultValue="0"
                    defaultValueName="..."
                    fieldData={props.users}
                    fieldName="user"
                    setValueForFilter={props.setValueForFilter}
                />
            </td>
            <td widtd="9%">
                <SelectField
                    defaultValue="---"
                    defaultValueName="..."
                    fieldData={props.statuses}
                    fieldName="status"
                    setValueForFilter={props.setValueForFilter}
                />
            </td>
        </tr>
    );
};
