import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheckCircle, faUserPlus, faFile, faComments, faReplyAll } from '@fortawesome/free-solid-svg-icons'
import { STATUS_NOT_ANSWER, STATUS_ANSWERED, STATUS_SWITCHED, STATUS_MADE_AN_APPOINTMENT, STATUS_ISSUANCE_DOCUMENTS } from '../../enum/enum'


export const IconsStatus = ({ idObjectCall, idCall, idStatus, ChangeStatusCall }) => {

    return (
        <div className="iconsStatus">
            {
                idStatus === STATUS_ANSWERED || idStatus === STATUS_NOT_ANSWER ?
                    <FontAwesomeIcon
                        title="Обработано"
                        icon={faCheckCircle}
                        className={idStatus === STATUS_NOT_ANSWER ? "text-success faCheckCircle" : "text-success"}
                        size="lg"
                        onClick={
                            idStatus === STATUS_NOT_ANSWER ?
                                () => { ChangeStatusCall(idObjectCall, idCall, STATUS_ANSWERED) }
                                : null
                        }
                    />
                    : null
            }
            {idStatus === STATUS_SWITCHED || idStatus === STATUS_NOT_ANSWER ?
                <FontAwesomeIcon
                    title="Переключен на специалиста"
                    icon={faUserPlus}
                    className={idStatus === STATUS_NOT_ANSWER ? "text-primary faUserPlus" : "text-primary"}
                    size="lg"
                    onClick={
                        idStatus === STATUS_NOT_ANSWER ?
                            () => { ChangeStatusCall(idObjectCall, idCall, STATUS_SWITCHED) }
                            : null
                    }
                />
                : null
            }
            {idStatus === STATUS_MADE_AN_APPOINTMENT || idStatus === STATUS_NOT_ANSWER ?
                <FontAwesomeIcon
                    title="Записан на прием"
                    icon={faComments}
                    className={idStatus === STATUS_NOT_ANSWER ? "text-secondary faComments" : "text-secondary"}
                    size="lg"
                    onClick={
                        idStatus === STATUS_NOT_ANSWER ?
                            () => { ChangeStatusCall(idObjectCall, idCall, STATUS_MADE_AN_APPOINTMENT) }
                            : null
                    }
                />
                : null}
            {
                idStatus === STATUS_ISSUANCE_DOCUMENTS || idStatus === STATUS_NOT_ANSWER ?
                    <FontAwesomeIcon
                        title="Записан на выдачу документов"
                        icon={faFile}
                        className={idStatus === STATUS_NOT_ANSWER ? "text-warning faFile" : "text-warning"}
                        size="lg"
                        onClick={
                            idStatus === STATUS_NOT_ANSWER ?
                                () => { ChangeStatusCall(idObjectCall, idCall, STATUS_ISSUANCE_DOCUMENTS) }
                                : null
                        }
                    /> : null
            }
            {idStatus !== STATUS_NOT_ANSWER && ChangeStatusCall !== undefined ?
                <FontAwesomeIcon
                    title="Поменять статус"
                    icon={faReplyAll}
                    size="lg"
                    onClick={() => { ChangeStatusCall(idObjectCall, idCall, STATUS_NOT_ANSWER) }}
                />
                : null}
        </div>
    )
}
