import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFilter } from "@fortawesome/free-solid-svg-icons";

export const TableHeader = ({ isFilter, setFilter }) => {
    return (
        <tr>
            <th width="7%">
                <FontAwesomeIcon
                    title={
                        !isFilter
                            ? "Нажмите для отображения фильтра"
                            : "Нажмите чтобы скрыть фильтр"
                    }
                    icon={faFilter}
                    size="lg"
                    className={isFilter ? "activeFilter" : "inActiveFilter"}
                    onClick={() => {
                        setFilter(!isFilter);
                    }}
                />
            </th>
            <th width="7%">Код СП</th>
            <th width="10%">Дата</th>
            <th width="24%">ФИО</th>
            <th width="14%">Телефон</th>
            <th width="10%">Тип контрагента</th>
            <th width="20%">Причина обращения</th>
            <th width="15%">Примечание</th>
            <th width="9%">Пользователь</th>
            <th width="9%">Статус</th>
        </tr>
    );
};
