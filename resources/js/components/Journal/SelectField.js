import React from 'react'

export const SelectField = ({ fieldData, fieldName, defaultValue, defaultValueName, setValueForFilter }) => {
   
    return (
        <select
            className="form-control"
            onChange={event => setValueForFilter(fieldName, event.target.value)}
        >
            {defaultValue ? <option value={defaultValue}>{defaultValueName}</option> : null}
            {
                fieldData.map((option, index) => {
                    return (
                        <option
                            key={index}
                            value={option.id}
                            defaultValue={option.id}
                        >
                            {option.name}
                        </option>
                    )
                })
            }
        </select>
    )
}
