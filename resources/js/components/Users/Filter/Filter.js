import React from 'react'
import { Formik, Form, Field } from 'formik'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser, faHome, faIdCard } from '@fortawesome/free-solid-svg-icons'
import './Filter.scss'
import { KEY_FOR_OPTION_MANUAL } from '../../../enum/enum'


const Filter = ({ upfrList, getUsersList, resetFilter }) => {

    return (

        <div className="filter wrap">
            <h3>Фильтр</h3>
            <Formik
                initialValues={{
                    login: '',
                    fio: '',
                    kodUpfr: '',
                    isResetFilterButton: false
                }}

                onSubmit={async (values, { setSubmitting, resetForm }) => {

                    !values.isResetFilterButton ?
                        getUsersList(
                            values.login,
                            values.fio,
                            values.kodUpfr)
                        : (
                            resetForm(),
                            resetFilter()
                        )
                    setSubmitting(false)
                }}
                render={({ values, isSubmitting, setFieldValue, submitForm }) => (
                    < Form >
                        <div className="subFilterBlock">

                            <div className="iconInput">
                                <FontAwesomeIcon icon={faIdCard} size="lg" />
                                <Field
                                    name="login"
                                    placeholder="Введите логин"
                                    type="text"
                                    value={values.login}
                                />
                            </div>

                            <div className="iconInput">
                                <FontAwesomeIcon icon={faUser} size="lg" />
                                <Field
                                    name="fio"
                                    placeholder="Введите ФИО"
                                    type="text"
                                    value={values.fio}
                                />
                            </div>
                            <div className="iconInput">

                                <FontAwesomeIcon icon={faHome} size="lg" />
                                <Field
                                    component="select"
                                    name="kodUpfr"
                                >
                                    <option
                                        key={KEY_FOR_OPTION_MANUAL}
                                        value=''>
                                        По всем подразделениям
                                        </option>
                                    {
                                        upfrList != undefined ?

                                            upfrList.map((option, index) => {
                                                return (
                                                    <option
                                                        key={index}
                                                        value={option.id_upfr}>
                                                        {option.name_upfr}
                                                    </option>
                                                )
                                            })
                                            : null
                                    }
                                </Field>
                            </div>
                        </div>
                        <div className="btnsFilter">
                            <button id="search"
                                className="btn-primary"
                                type="button"
                                onClick={async () => {
                                    await setFieldValue('isResetFilterButton', false);
                                    submitForm();
                                }
                                }>
                                Поиск
                            </button>
                            <button id="resetFilter"
                                className="btn-success"
                                type="button"
                                onClick={async () => {
                                    await setFieldValue('isResetFilterButton', true);
                                    submitForm();
                                }
                                }>
                                Показать всех
                            </button>
                        </div>
                    </Form>
                )}
            />
        </div>
    )
}


export default Filter
