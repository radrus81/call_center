import React from 'react'
import { Field } from 'formik'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHome } from '@fortawesome/free-solid-svg-icons'


export const FieldUpfr = ({ errorUpfr, isInEditMode, upfrList }) => (
    <>
        {
            errorUpfr &&
            <div className="messErr alert-danger">{errorUpfr}</div>
        }
        <div className="iconInput">
            <FontAwesomeIcon icon={faHome} size="lg" />
            <Field component="select" name="kodUpfr">
                {
                    isInEditMode ? null : <option key={100} value='---'>---</option>
                }
                {
                    upfrList != undefined ?
                        upfrList.map((option, index) => {
                            return (
                                <option
                                    key={index}
                                    value={option.id_upfr}>
                                    {option.name_upfr}
                                </option>
                            )
                        })
                        : null
                }
            </Field>
        </div>
    </>
)
