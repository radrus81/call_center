import React from 'react'
import { Field } from 'formik'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser } from '@fortawesome/free-solid-svg-icons'


export const FieldUserFullName = ({ errorUserFullName, valueUserFullName, touchedUserFullName }) => (
    <>
        {
            errorUserFullName && touchedUserFullName &&
            <div className="messErr alert-danger">{errorUserFullName}</div>
        }
        <div className="iconInput">
            <FontAwesomeIcon icon={faUser} size="lg" />
            <Field
                name="userFullName"
                placeholder="Введите ФИО"
                type="text"
                value={valueUserFullName || ''}
            />
        </div>
    </>
)
