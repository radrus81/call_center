import React from 'react'
import { Field } from 'formik'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faDatabase } from '@fortawesome/free-solid-svg-icons'

import { ACCESS_USER_UPFR } from '../../../../enum/enum'

export const FieldAccess = ({ errorAccess, isInEditMode, listAccess }) => (
    <>
        {
            errorAccess &&
            <div className="messErr alert-danger">{errorAccess}</div>
        }
        <div className="iconInput">
            <FontAwesomeIcon icon={faDatabase} size="lg" />
            <Field component="select" name="access">
                {
                    isInEditMode ? null : <option key={100} value='---'>---</option>
                }
                {
                    listAccess != undefined ?
                        listAccess.map((option, index) => {
                            return (
                                <option
                                    key={index}
                                    value={option.id_dostup}>
                                    {option.name_dostup}
                                </option>
                            )
                        })
                        : null
                }
            </Field>
        </div>
    </>
)
