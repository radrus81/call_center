import React from 'react'
import { Label } from 'reactstrap';
import { Checkbox } from 'react-formik-ui'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStopCircle } from '@fortawesome/free-solid-svg-icons'


export const FieldIsBlocked = ({ valueIsBlocked }) => (

    <div className="iconInput">
        <FontAwesomeIcon icon={faStopCircle} size="lg" />
        <Checkbox
            className='checkIsBlocked'
            name="isBlocked"
            value={valueIsBlocked} />
        <Label>{valueIsBlocked ? 'Заблокирован' : 'Активен'}</Label>
    </div>
)


