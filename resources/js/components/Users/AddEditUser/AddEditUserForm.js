import React from 'react'
import { Formik } from 'formik'
import { Form } from 'react-formik-ui'
import AddUserFormSchema from './AddUserFormSchema'
import EditUserFormSchema from './EditUserFormSchema'
import './AddEditUserForm.scss'
import { FieldLogin } from './Fields/FieldLogin'
import { FieldPassword } from './Fields/FieldPassword';
import { FieldUserFullName } from './Fields/FieldUserFullName'
import { FieldUpfr } from './Fields/FieldUpfr'
import { FieldAccess } from './Fields/FieldAccess'
import { FieldIsBlocked } from './Fields/FieldIsBlocked'
import { FieldButtons } from './Fields/FieldButtons'


const AddEditUserForm = ({ state, showFieldPasswordEdit, addUser, updateUser, upfrList, listAccess, cancelEditUser }) => {

    return (

        <div className="wrap">
            <h3>{state.isInEditMode ? 'Редактировать пользователя' : 'Добавить пользователя'}</h3>
            <Formik
                initialValues={state}
                enableReinitialize={true}
                validationSchema={state.isInEditMode ? EditUserFormSchema : AddUserFormSchema}
                onSubmit={async (values, { resetForm, setSubmitting }) => {

                    !state.isInEditMode ?
                        addUser(
                            values.login,
                            values.pass,
                            values.userFullName,
                            values.kodUpfr,
                            values.access,
                            values.isBlocked
                        )
                        :
                        updateUser(
                            state.id,
                            values.login,
                            values.pass,
                            values.userFullName,
                            values.kodUpfr,
                            values.access,
                            values.isBlocked
                        )
                    resetForm()
                    setSubmitting(false)
                }}
                render={({ errors, touched, values, isSubmitting }) => (

                    < Form >
                        <div className="subFilterBlock">
                            <FieldLogin
                                errorLogin={errors.login}
                                valueLogin={values.login}
                                touchedLogin={touched.login}
                            />
                            <FieldPassword
                                errorPass={errors.pass}
                                valuePass={values.pass}
                                touchedPass={touched.pass}
                                isInEditMode={state.isInEditMode}
                                showFieldPasswordEdit={showFieldPasswordEdit}
                            />
                            <FieldUserFullName
                                errorUserFullName={errors.userFullName}
                                valueUserFullName={values.userFullName}
                                touchedUserFullName={touched.userFullName}
                            />
                            <FieldUpfr
                                errorUpfr={errors.kodUpfr}
                                isInEditMode={state.isInEditMode}
                                upfrList={upfrList}
                            />
                            <FieldAccess
                                errorAccess={errors.access}
                                isInEditMode={state.isInEditMode}
                                listAccess={listAccess}
                            />
                            <FieldIsBlocked
                                valueIsBlocked={values.isBlocked}
                            />
                        </div>
                        <FieldButtons
                            isSubmitting={isSubmitting}
                            isInEditMode={state.isInEditMode}
                            cancelEditUser={cancelEditUser}
                        />
                    </Form>
                )}
            />
        </div>
    )
}


export default AddEditUserForm
