import * as Yup from "yup";

const EditUserFormSchema = Yup.object().shape({
    login: Yup.string()
        .required("Не заполнено поле"),
    userFullName: Yup.string()
        .required("Не заполнено поле")
        .matches(/^(?! )(?!.* $)((?:.* ){2}).+$/, 'Неверное ФИО. Должен быть: Ф И О '),
    kodUpfr: Yup.string().matches(/(^[0-9]+$)/, 'Не выбран район'),
    access: Yup.string().matches(/(^[0-9]+$)/, 'Не выбран доступ')
});

export default EditUserFormSchema
