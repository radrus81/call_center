import React, { Component } from 'react'
import { Table } from 'reactstrap'
import './Users.scss'
import Filter from './Filter/Filter'
import AddEditUserForm from './AddEditUser/AddEditUserForm'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPencilAlt } from '@fortawesome/free-solid-svg-icons'



class Users extends Component {

    constructor(props) {
        super(props);
        this.setUserForEdit = this.setUserForEdit.bind(this)
        this.cancelEditUser = this.cancelEditUser.bind(this)
        this.updateUser = this.updateUser.bind(this)
        this.resetFilter = this.resetFilter.bind(this)


        this.state = {
            id_obj: null,
            login: '',
            pass: '',
            userFullName: '',
            kodUpfr: '---',
            access: '---',
            id: 0,
            isInEditMode: false,
            isBlocked: 0
        };
    }

    componentDidMount() {

        let login = ''
        let userFullName = ''
        let kodUpfr = ''
        this.props.getUsersList(login, userFullName, kodUpfr)
        this.props.getUpfrList()
    }

    setUserForEdit(id_obj, id) {
        const actualUser = this.props.usersList[id_obj]
        //утановим значения в локальный state, тем самым заполним поля данными пользователя
        this.setState({
            id_obj: id_obj,
            login: actualUser.login,
            userFullName: actualUser.userFullName,
            kodUpfr: actualUser.kod_upfr,
            access: actualUser.id_dostup,
            id: id,
            isInEditMode: true,
            isBlocked: actualUser.isBlocked
        });
    }

    updateUser(id, login, pass, userFullName, upfrCode, access, isBlocked) {
        this.props.updateUser(this.state.id_obj, id, login, pass, userFullName, upfrCode, access, isBlocked)
        this.cancelEditUser()
    }

    cancelEditUser() {
        this.setState({
            id_obj: null,
            login: '',
            pass: '',
            userFullName: '',
            kodUpfr: '---',
            access: '---',
            isInEditMode: false,
            isBlocked: 0
        });
    }

    resetFilter() {
        let login = ''
        let userFullName = ''
        let kodUpfr = ''
        this.props.getUsersList(login, userFullName, kodUpfr)
    }

    render() {

        return (
            <section className="blockUsers wrap">
                <hr />
                <h3>Пользователи</h3>
                <hr />
                {
                    this.props.errorMessage !== "" ?
                        <div className="messErrAll alert-danger">{this.props.errorMessage}</div>
                        : null
                }
                {
                    this.props.successMessage !== "" ?
                        <div className="messErrAll alert-success">{this.props.successMessage}</div>
                        : null
                }
                <div className="blockUsers_control">
                    <div className="filterUser">
                        <Filter
                            upfrList={this.props.upfrList}
                            getUsersList={this.props.getUsersList}
                            resetFilter={this.resetFilter}
                        />
                    </div>
                    <div className="addUser">
                        <AddEditUserForm
                            showFieldPasswordEdit={this.props.showFieldPasswordEdit}
                            listAccess={this.props.listAccess}
                            upfrList={this.props.upfrList}
                            addUser={this.props.addUser}
                            updateUser={this.updateUser}
                            state={this.state}
                            cancelEditUser={this.cancelEditUser}
                        />
                    </div>
                </div>
                <div className="usersTable">
                    <Table >
                        <thead className="thead-dark">
                            <tr className="firstTr">
                                <th></th>
                                <th>id_user</th>
                                <th>Логин</th>
                                <th>ФИО</th>
                                <th>УПФР</th>
                                <th>Доступ</th>
                                <th>Статус</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.props.usersList != undefined ?

                                    this.props.usersList.map((tr, index) => {
                                        return (
                                            <tr key={index}>
                                                <td><div title="Редактировать" className="editIcon" onClick={() => { this.setUserForEdit(index, tr.id) }}><FontAwesomeIcon icon={faPencilAlt} size="lg" /></div></td>
                                                <td>{tr.id}</td>
                                                <td>{tr.login}</td>
                                                <td>{tr.userFullName}</td>
                                                <td>{tr.name_upfr}</td>
                                                <td>{tr.name_dostup}</td>
                                                <td>{tr.isBlocked == 0 ? 'Активен' : 'Заблокирован'}</td>
                                            </tr>
                                        )
                                    })
                                    : null
                            }
                        </tbody>
                    </Table>
                </div>
            </section>
        )
    }
}

export default Users
