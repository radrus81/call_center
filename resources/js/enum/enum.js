export const ACCESS_ADMIN = 1
export const ACCESS_USER_OPFR = 2
export const ACCESS_USER_UPFR_ALL = 3
export const ACCESS_USER_UPFR = 4
export const ACCESS_USER_SECURITY_INFO = 5
export const ACCESS_USER_CONTROLLER_OPFR = 6

export const STATUS_NOT_ANSWER = 0
export const STATUS_ANSWERED = 1
export const STATUS_SWITCHED = 2
export const STATUS_MADE_AN_APPOINTMENT = 3
export const STATUS_ISSUANCE_DOCUMENTS = 4

export const KEY_FOR_OPTION_MANUAL = 100;
