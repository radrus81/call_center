<?php

namespace App\Services\SecretWord;

use Illuminate\Support\Facades\DB;

class SecretWordService
{
    public function __construct()
    {
        $this->snils = '';
    }

    public function getQuestionAndAnswer($snils)
    {
        if (!$this->checkSnils($snils)) {
            return  array(['base' => 'Ошибка', 'createDate' => 'Ошибка', 'question' => 'Неправильный СНИЛС', 'answer' => 'Исправьте']);
        } else {
            $this->snils = $snils;
            $snils = str_replace("-", "", str_replace(" ", "", $snils));
            return $this->getSecretWordFromPtkKs($snils);
        }
    }

    public function wintoutf($text)
    {
        return iconv('CP1251', 'UTF-8',  $text);
        return $text;
    }

    private function getSecretWordFromPtkKs($snils)
    {
        setlocale(LC_ALL, 'ru_RU.UTF-8');
        $database = "DRIVER={" . env('DRIVER_ODBC_CENTOC', 'DB2_CLI') . "};
                        PROTOCOL=" . env('PROTOCOL_ODBC_CENTOC', 'TCPIP') . ";
                        CURRENTSCHEME=" . env('CURRENTSCHEME_ODBC_CENTOC', 'DB2ADMIN') . ";
                        HOSTNAME=" . env('HOSTNAME_ODBC_CENTOC', '') . ";
                        PORT=" . env('PORT_ODBC_CENTOC', '50000') . ";
                        DATABASE=" . env('DATABASE_ODBC_CENTOC', 'CSERVICE') . ";
                        UID=" . env('UID_ODBC_CENTOC', '') . ";
                        PWD=" . env('PWD_ODBC_CENTOC', '') . ";";
        $username = "";
        $password = "";

        if (PHP_OS == 'WINNT') {
            $DB_connect = odbc_connect(
                env('ODBC_DATA_SOURCE_NAME_WINDOWS', ''),
                env('ODBC_USER_ID_WINDOWS', ''),
                env('ODBC_WIMDOWS_PASSWORD', '')
            );
        } else {
            $DB_connect  = odbc_connect($database, $username, $password);
        }

        if (!$DB_connect) {
            return array(['base' => 'ПТК КС', 'createDate' => 'Не смог подключиться к базе', 'question' => '', 'answer' => '']);
        }
        $sql = "select
                coalesce(c.value,'') as Snils
                ,coalesce((select value from DB2ADMIN.V_COMPONENT_VALUE tmp_c
                    where
                    tmp_c.id_reg=c.ID_REG
                    and tmp_c.NAME='dateAddress'),'') as setat
                ,coalesce((select value from DB2ADMIN.V_COMPONENT_VALUE tmp_c
                    where
                    tmp_c.id_reg=c.ID_REG
                    and tmp_c.NAME='controlInfo'),'') as controlInfo
                ,coalesce((select value from DB2ADMIN.V_COMPONENT_VALUE tmp_c
                    where
                    tmp_c.id_reg=c.ID_REG
                    and tmp_c.NAME='controlInfoResponse'),'') as controlInfoResponse
                ,coalesce((select value from DB2ADMIN.V_COMPONENT_VALUE tmp_c
                    where
                    tmp_c.id_reg=c.ID_REG
                    and tmp_c.NAME='secretCode'),'') as secretCode
                from DB2ADMIN.V_COMPONENT_VALUE c
                where c.NAME='persnum'
                and c.value='" . $snils . "' union all				
				select
                coalesce(c.value,'') as Snils
                ,coalesce((select value from (select ARCHIVE.R_COMPONENTS_VALUE.* from ARCHIVE.R_COMPONENTS_VALUE, DB2ADMIN.S_COMPONENTS  
		where DB2ADMIN.S_COMPONENTS.ID_components=ARCHIVE.R_COMPONENTS_VALUE.COMPONENTS_ID_COMPONENTS_OID and DB2ADMIN.S_COMPONENTS.NAME='dateAddress') tmp_c
                    where tmp_c.REGISTER_ID_OID=c.REGISTER_ID_OID),'') as setat
                ,coalesce((select value from(select ARCHIVE.R_COMPONENTS_VALUE.* from ARCHIVE.R_COMPONENTS_VALUE, DB2ADMIN.S_COMPONENTS  
		where DB2ADMIN.S_COMPONENTS.ID_components=ARCHIVE.R_COMPONENTS_VALUE.COMPONENTS_ID_COMPONENTS_OID and DB2ADMIN.S_COMPONENTS.NAME='controlInfo') tmp_c
                    where tmp_c.REGISTER_ID_OID=c.REGISTER_ID_OID),'') as controlInfo
        ,coalesce((select value from(select ARCHIVE.R_COMPONENTS_VALUE.* from ARCHIVE.R_COMPONENTS_VALUE, DB2ADMIN.S_COMPONENTS  
		where DB2ADMIN.S_COMPONENTS.ID_components=ARCHIVE.R_COMPONENTS_VALUE.COMPONENTS_ID_COMPONENTS_OID and DB2ADMIN.S_COMPONENTS.NAME='controlInfoResponse') tmp_c
                    where tmp_c.REGISTER_ID_OID=c.REGISTER_ID_OID),'')
        as controlInfoResponse
        ,coalesce((select value from (select ARCHIVE.R_COMPONENTS_VALUE.* from ARCHIVE.R_COMPONENTS_VALUE, DB2ADMIN.S_COMPONENTS  
		where DB2ADMIN.S_COMPONENTS.ID_components=ARCHIVE.R_COMPONENTS_VALUE.COMPONENTS_ID_COMPONENTS_OID and DB2ADMIN.S_COMPONENTS.NAME='secretCode') tmp_c
                    where tmp_c.REGISTER_ID_OID=c.REGISTER_ID_OID),'') as secretCode
                from (select ARCHIVE.R_COMPONENTS_VALUE.* from ARCHIVE.R_COMPONENTS_VALUE, DB2ADMIN.S_COMPONENTS  
	    where DB2ADMIN.S_COMPONENTS.ID_components=ARCHIVE.R_COMPONENTS_VALUE.COMPONENTS_ID_COMPONENTS_OID and DB2ADMIN.S_COMPONENTS.NAME='persnum') c
                where c.value='" . $snils . "'";


        $resultset = odbc_exec($DB_connect, $sql);

        $dataFromPtkKs = array();
        $question = '';
        $answer = '';
        while ($row = odbc_fetch_array($resultset)) {

            if (($this->wintoutf($row['CONTROLINFO']) != '') && (($this->wintoutf($row['CONTROLINFORESPONSE']) != '') || ($this->wintoutf($row['SECRETCODE']) != ''))) {
                switch ($this->wintoutf($this->wintoutf($row['CONTROLINFO']))) {
                    case '1':
                        $question = 'Девичья фамилия матери?';
                        if ($this->wintoutf($row['CONTROLINFORESPONSE']) != '') {
                            $answer = $this->wintoutf($row['CONTROLINFORESPONSE']);
                        } else {
                            $answer = $this->wintoutf($row['SECRETCODE']);
                        }
                        break;
                    case '2':
                        $question = 'Кличка домашнего питомца?';
                        if ($this->wintoutf($row['CONTROLINFORESPONSE']) != '') {
                            $answer = $this->wintoutf($row['CONTROLINFORESPONSE']);
                        } else {
                            $answer = $this->wintoutf($row['SECRETCODE']);
                        }
                        break;
                    case '3':
                        $question = 'Номер школы, которую Вы закончили?';
                        if ($this->wintoutf($row['CONTROLINFORESPONSE']) != '') {
                            $answer = $this->wintoutf($row['CONTROLINFORESPONSE']);
                        } else {
                            $answer = $this->wintoutf($row['SECRETCODE']);
                        }
                        break;
                    case '4':
                        $question = 'Любимое блюдо?';
                        if ($this->wintoutf($row['CONTROLINFORESPONSE']) != '') {
                            $answer = $this->wintoutf($row['CONTROLINFORESPONSE']);
                        } else {
                            $answer = $this->wintoutf($row['SECRETCODE']);
                        }
                        break;
                    case '5':
                        $question = 'Ваш любимый писатель?';
                        if ($this->wintoutf($row['CONTROLINFORESPONSE']) != '') {
                            $answer = $this->wintoutf($row['CONTROLINFORESPONSE']);
                        } else {
                            $answer = $this->wintoutf($row['SECRETCODE']);
                        }
                        break;
                    case '-1':
                        if (($this->wintoutf($row['SECRETCODE']) != '') && ($this->wintoutf($row['CONTROLINFORESPONSE']) != '')) {
                            $question = $this->wintoutf($row['CONTROLINFORESPONSE']);
                            $answer = $this->wintoutf($row['SECRETCODE']);
                        } else {
                            $question = 'Секретный код?';
                            $answer = $this->wintoutf($row['SECRETCODE']);
                        }
                        break;
                    default:
                        echo ("Нет информации по коду № " + $this->wintoutf($row['CONTROLINFO']) + ' Сообщите администратору!');
                        break;
                }
                if ($answer == '') {
                    $answer = $this->wintoutf($row['CONTROLINFORESPONSE']);
                }
                $date_c = '';
                if (trim($row['SETAT'] != '')) {
                    $date_c = date('d.m.Y', strtotime($row['SETAT']));
                }
                array_push($dataFromPtkKs, ['base' => 'ПТК КС', 'createDate' => $date_c, 'question' => $question, 'answer' => $answer]);
            }
        }
        if (!count($dataFromPtkKs)) {
            array_push($dataFromPtkKs, ['base' => 'ПТК КС', 'createDate' => 'Нет данных', 'question' => 'Нет данных', 'answer' => 'Нет данных']);
        }
        odbc_close($DB_connect);

        if (env('CONNECT_TO_BASE_PORTAL_OPFR73', false) === true) {
            $dataSecretWord = $this->getSecretWordFromPortalOpfr($snils, $dataFromPtkKs);
            return $this->getSecretWordFromFrontOffice($dataSecretWord);
        } else {
            $dataSecretWord = $this->getSecretWordFromFrontOffice($snils, $dataFromPtkKs);
            return $dataSecretWord;
        }
    }

    private function getSecretWordFromFrontOffice($dataSecretWord)
    {
        $snils = $this->snils;
        $where = [['snils', $snils]];
        $isSnils = $this->getIsSnilsFromFrontOffice($where);
        if ($isSnils) {
            DB::table('secret_word_from_fo')->where('snils', $snils)->update(['status' => null]);
            $dataSecretWord = $this->awaitDataFromFO(false, $dataSecretWord, $snils);
        } else {
            DB::table('secret_word_from_fo')->insert(['snils' => $snils]);
            $dataSecretWord = $this->awaitDataFromFO(false, $dataSecretWord, $snils);
        }
        return $dataSecretWord;
    }


    private function awaitDataFromFO($isSnils, $dataSecretWord, $snils)
    {
        $time = 0;
        $where = [['snils', $snils], ['status', 1]];
        while (!$isSnils) {
            $isSnils = $this->getIsSnilsFromFrontOffice($where);
            sleep(1);
            $time++;
            if ($time === 5) {
                array_push($dataSecretWord, [
                    'base' => 'Фронт офис',
                    'createDate' => 'нет ответа',
                    'question' => 'от сервиса',
                    'answer' => 'обратитесь администратору'
                ]);
                return $dataSecretWord;
            }
        }
        array_push($dataSecretWord, [
            'base' => 'Фронт офис',
            'createDate' => $isSnils->created_at,
            'question' => $isSnils->control_question,
            'answer' => $isSnils->secret_word
        ]);
        return $dataSecretWord;
    }

    private function getIsSnilsFromFrontOffice($where)
    {
        return DB::table('secret_word_from_fo')
            ->select(DB::raw('
                    DATE_FORMAT(created_at, "%d.%m.%Y %H:%i:%s") as created_at,
                    control_question,secret_word'))
            ->where($where)
            ->first();
    }

    private function getSecretWordFromPortalOpfr($snils, $dataFromPtkKs)
    {
        $dataFromPortalOpfr = DB::connection('mysql_portal_opfr')
            ->table('journalregistration_statement')
            ->select(DB::raw('casesecret,question,answer, DATE_FORMAT(datecreate,"%d.%m.%Y") as datecreate'))
            ->where([
                ['snils', $snils],
                ['gtsvd', 1]
            ])
            ->orderBy('id', 'desc')
            ->offset(0)
            ->limit(1)
            ->get();

        if (count($dataFromPortalOpfr)) {
            return $this->getCasesecret(
                $dataFromPortalOpfr[0]->casesecret,
                $dataFromPortalOpfr[0]->question,
                $dataFromPortalOpfr[0]->answer,
                $dataFromPortalOpfr[0]->datecreate,
                $dataFromPtkKs
            );
        } else {
            array_push($dataFromPtkKs, [
                'base' => 'Портал ОПФР',
                'createDate' => 'Нет данных',
                'question' => 'Нет данных',
                'answer' => 'Нет данных'
            ]);
            return $dataFromPtkKs;
        }
    }

    private function getCasesecret($codeSecretQuestion, $question, $expectAnAnswer, $datecreate, $dataFromPtkKs)
    {
        switch ($codeSecretQuestion) {
            case '0':
                $secretQuestion = 'При регистрации не выбран вопрос для секретного слова.';
                break;
            case 'a':
                $secretQuestion = 'Девичья фамилия матери?';
                break;
            case 'b':
                $secretQuestion = 'Кличка домашнего питомца?';
                break;
            case 'c':
                $secretQuestion = 'Номер школы, которую Вы закончили?';
                break;
            case 'd':
                $secretQuestion = 'Любимое блюдо?';
                break;
            case 'e':
                $secretQuestion = 'Ваш любимый писатель?';
                break;
            case 'f':
                $secretQuestion = 'секретный код?';
                break;
            case 'g':
                $secretQuestion = $question;
                break;

            default:
                $secretQuestion = "Нет информации по коду №" + $codeSecretQuestion + ' Сообщите администратору!';
                break;
        }
        array_push(
            $dataFromPtkKs,
            [
                'base' => 'Портал ОПФР',
                'createDate' => $datecreate,
                'question' => $secretQuestion,
                'answer' => $expectAnAnswer
            ]
        );
        return $dataFromPtkKs;
    }


    private function checkSnils($snils)
    {
        preg_match("/(\d{3})-(\d{3})-(\d{3})\s{1}(\d{2})/", $snils, $matches);
        if ($matches == false) return false;
        $kontr = $matches[4];
        $num = $matches[1] . $matches[2] . $matches[3];
        $k = 9;
        $sum = 0;
        for ($i = 0; $i < 9; $i++) {
            $sum += $k * $num{
                $i};
            $k--;
        }
        if ($sum < 100) {
            if ($kontr == $sum) return true;
            else return false;
        } elseif ($sum == 100 || $sum == 101) {
            if ($kontr == '00') return true;
            else return false;
        } else {
            $ost = $sum % 101;
            if ($ost == $kontr) return true;
            elseif ($ost == 100 && $kontr == '00') return true;
            else  return false;
        }
    }
}
