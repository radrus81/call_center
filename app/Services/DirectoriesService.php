<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;

class DirectoriesService
{
    public function getCodesUpfr($access, $codeUpfr)
    {
        $codesUpfr = array();
        $upfrs = $this->getUpfr($access, $codeUpfr);
        foreach ($upfrs as $data) {
            array_push($codesUpfr, $data->id);
        }
        return $codesUpfr;
    }

    public function getUpfr($access, $codeUpfr)
    {
        $allUpfr = $this->getAllUpfr();
        $accessControllerOpfr = config('enums.ACCESS_USER_CONTROLLER_OPFR', 6);
        if ($access <= (config('enums.ACCESS_USER_OPFR')) || (int) $access === $accessControllerOpfr) {
            return $allUpfr;
        } else {
            $userUpfr = $this->getKsMru($codeUpfr);

            if (!count($userUpfr)) {
                return $this->getOnlyKs($codeUpfr);
            } else {
                return $userUpfr;
            }
        }
    }

    public function getAllUpfr()
    {
        return DB::table('upfr')
            ->select("id_upfr as id", "name_upfr as name")
            ->get();
    }

    public function getKsMru($codeUpfr)
    {
        return DB::table('upfr')
            ->select("id_upfr as id", "name_upfr as name")
            ->where('id_mru', $codeUpfr)
            ->get();
    }

    public function getOnlyKs($codeUpfr)
    {
        return  DB::table('upfr')
            ->select("id_upfr as id", "name_upfr as name")
            ->where('id_upfr', $codeUpfr)
            ->get();
    }

    public function getAreaOfDislocation()
    {
        return DB::table('raion')
            ->select("id", "nazv as name")
            ->get();
    }

    public function getCounterparties()
    {
        return DB::table('kontragent')
            ->select("id", "name")
            ->get();
    }

    public function getTopics()
    {
        return DB::table('topics')
            ->select("id", "topic as name")
            ->get();
    }

    public function getSubTopics()
    {
        return DB::table('subtopics')
            ->select("subtopics.id", "subtopics.subtopic as name", "topics.topic", "subtopics.idtopic")
            ->leftjoin('topics', 'topics.id', '=', 'subtopics.idtopic')
            ->get();
    }

    public function getOldTopic()
    {
        return DB::table('tema')
            ->select("id", "name")
            ->get();
    }

    public function getTypeConsultation()
    {
        return DB::table('typeconsultation')
            ->select("id", "type as name")
            ->get();
    }

    public function getResultConsultation()
    {
        return DB::table('resultconsultation')
            ->select("id", "result as name")
            ->get();
    }



    public function getUsers($access, $codeUpfr)
    {
        $codeKsMru = array();
        $accessAdmin = config('enums.ACCESS_ADMIN');
        $accessControllerOpfr = config('enums.ACCESS_USER_CONTROLLER_OPFR', 6);
        if (in_array((int) $access, [$accessAdmin, $accessControllerOpfr])) {
            $userUpfr = $this->getAllUpfr();
            foreach ($userUpfr as $data) {
                array_push($codeKsMru, $data->id);
            }
        } else {
            $userUpfr = $this->getKsMru($codeUpfr);
            if (!count($userUpfr)) {
                array_push($codeKsMru, $codeUpfr);
            } else {
                foreach ($userUpfr as $data) {
                    array_push($codeKsMru, $data->id);
                }
            }
        }
        return DB::table('users')
            ->select("id", "fio as name")
            ->where('isBlocked', '=', false)
            ->whereNotIn('dostup', [config('enums.ACCESS_USER_OZI')])
            ->whereIn('kod_upfr', $codeKsMru)
            ->orderBy('fio')
            ->get();
    }

    public function getStatuses()
    {
        return DB::table('status')
            ->select("id_status as id", "name_status as name")
            ->get();
    }
}
