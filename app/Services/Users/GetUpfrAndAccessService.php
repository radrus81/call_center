<?php

namespace App\Services\Users;


use Illuminate\Support\Facades\DB;

class GetUpfrAndAccessService
{
    public function getUpfrList()
    {
        return DB::table('upfr')
            ->select('id_upfr', 'name_upfr')
            ->orderBy('id_upfr')
            ->get();
    }
    public function getAccessList()
    {
        return DB::table('dostup')
            ->select('id_dostup', 'name_dostup')
            ->get();
    }
}
