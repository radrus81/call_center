<?php

namespace App\Services\Users;


use Illuminate\Support\Facades\DB;

class GetDataUsersService
{
    public function getDataFromTableUsers($request)
    {
        $login = $request->get('login');
        $fio = $request->get('fio');
        $kodUpfr = $request->get('kodUpfr');

        $where = array();
        //если поле фио заполнено
        if (trim($fio) != '') {
            $where[] = ['fio', 'like', '%' . $fio . '%'];
        }
        //если поле снилс заполнено
        if (trim($login) != '') {
            $where[] = ['login', 'like', '%' . $login . '%'];
        }
        //если выбрана закладка "мои запис по показать только записи конкетного спеца"
        if ($kodUpfr != '') {
            $where[] =  ['kod_upfr', '=', $kodUpfr];
        }
        if (count($where) == 0) {
            $where[] = ['kod_upfr', '>', 0];
        }

        return $this->getDataUsers($where);
    }

    private function getDataUsers($where)
    {
        return (DB::table('users')
            ->select(
                'users.id',
                'users.login',
                'upfr.name_upfr',
                'dostup.name_dostup',
                'dostup.id_dostup as id_dostup',
                'users.fio as userFullName',
                'users.kod_upfr',
                'users.isBlocked'
            )
            ->leftJoin('upfr', 'upfr.id_upfr', '=', 'users.kod_upfr')
            ->leftJoin('dostup', 'dostup.id_dostup', '=', 'users.dostup')
            ->where($where)
            ->orderBy('users.id')
            ->get());
    }
}
