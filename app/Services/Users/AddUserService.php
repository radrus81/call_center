<?php

namespace App\Services\Users;

use Illuminate\Support\Facades\DB;
use App\Services\Users\UpdateUserService;

class AddUserService
{
    public function __construct(
        UpdateUserService $updateUserService
    ) {
        $this->dataUser = $updateUserService;
    }


    public function addUser($request)
    {
        if (!$this->isLoginFromDB($request->login)) {
            $insert = array(
                'login' => $request->login, 'pass' => $request->password, 'kod_upfr' => $request->kod_upfr,
                'fio' => $request->fio, 'dostup' => $request->access, 'isBlocked' => $request->isBlocked
            );
            $newId = DB::table('users')->insertGetId($insert);
            $resultAdd = array('resultCode' => 200, 'dataUser' => $this->dataUser->getDataUser($newId));
        } else {
            $resultAdd = array('resultCode' => 422, 'dataUser' => []);
        }
        return $resultAdd;
    }

    private function isLoginFromDB($login)
    {
        return DB::table('users')
            ->select('users.id as count')
            ->where('login', $login)
            ->count();
    }
}
