<?php

namespace App\Services\HistoryCalls;

use Illuminate\Support\Facades\DB;
use App\Services\HelpersService;


class GetHistoryCallsService
{

    public function __construct(HelpersService $helpersService)
    {
        $this->helpersService = $helpersService;
    }

    public function getHistoryCalls($page, $fullNamePensioner)
    {
        $where = array();
        $partsFullName = $this->helpersService->getPartsFromFullName($fullNamePensioner);
        $where[] = ['calls.fam', 'like', '%' . $partsFullName['family'] . '%'];
        if (!empty($partsFullName['name'])) {
            $where[] = ['calls.name', 'like', '%' . $partsFullName['name'] . '%'];
        }
        if (!empty($partsFullName['father'])) {
            $where[] = ['calls.otch', 'like', '%' . $partsFullName['father'] . '%'];
        }
        return array('totalCount' => $this->getTotalCount($where),  'dataCalls' => $this->getSql($page, $where));
    }



    private function getSql($page, $where)
    {

        return DB::table('calls')
            ->select(
                DB::raw("calls.id as idCall,calls.kod_upfr as codeUpfr,
            DATE_FORMAT(FROM_UNIXTIME(calls.time_start), '%d.%m.%Y %H:%i:%s') as dateStart,
            calls.fam as familyClient,
            calls.name as nameClient,
            calls.otch as middleNameClient,
            COALESCE(calls.snils,'') as snils,
            COALESCE(calls.phone,'') as phoneClient,
            raion.id as idRaion,
            raion.nazv as areaOfDislocation,
            kontragent.id as idKontragent, 
            kontragent.name as typeOfCounterparty,
            COALESCE(tema.name,'') as nameTopicOld,
            COALESCE(topics.topic,'') as topic,
            COALESCE(subtopics.subtopic,'') as subtopic,
            COALESCE(typeconsultation.type,'')  as typeConsultation,
            COALESCE(resultconsultation.result,'')  as resultConsultation,
            COALESCE(calls.prim,'') as notice,
            users.fio as nameUser,
            if (calls.kod_otvet=1, 'да' , if (calls.kod_otvet=0,'','нет')) as secretWord,
            COALESCE(upfr.name_upfr,'нет жалобы') as complaintСodeUpfr,
            if (DATE_FORMAT(calls.dateOfIssue, '%d.%m.%Y') = '00.00.0000','',DATE_FORMAT(calls.dateOfIssue, '%d.%m.%Y')) as dateOfIssue,
            COALESCE(calls.complaintDescription,'') as complaintDescription,
            status.id_status as statusName,
            calls.status")
            )
            ->leftjoin('raion', 'raion.id', '=', 'calls.raion')
            ->leftjoin('tema', 'tema.id', '=', 'calls.tema')
            ->leftjoin('status', 'status.id_status', '=', 'calls.status')
            ->leftjoin('topics', 'topics.id', '=', 'calls.topic')
            ->leftjoin('subtopics', 'subtopics.id', '=', 'calls.subtopic')
            ->leftjoin('typeconsultation', 'typeconsultation.id', '=', 'calls.typeconsult')
            ->leftjoin('resultconsultation', 'resultconsultation.id', '=', 'calls.resultconsult')
            ->leftjoin('kontragent', 'kontragent.id', '=', 'calls.type')
            ->leftjoin('users', 'users.id', '=', 'calls.user')
            ->leftjoin('upfr', 'calls.complaintСodeUpfr', '=', 'upfr.id_upfr')
            ->where($where)
            ->orderBy('calls.id', 'desc')
            ->offset($this->getOffset($page))
            ->limit(10)
            ->get();
    }

    private function getTotalCount($where)
    {
        return DB::table('calls')
            ->select('id')
            ->where($where)
            ->count();
    }

    private function getOffset($page)
    {
        $pageSize = config('enums.PAGE_SIZE_FOR_HISTORY_CALLS', 10);
        if ($page) {
            return ($page * $pageSize) - $pageSize;
        } else {
            return 0;
        }
    }
}
