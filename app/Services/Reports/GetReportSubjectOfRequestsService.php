<?php

namespace App\Services\Reports;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Services\HelpersService;
use App\Services\DirectoriesService;


class GetReportSubjectOfRequestsService
{
    protected $arraySubjectOfRequests;
    protected $arrayResultConsult;

    public function __construct(DirectoriesService $directoriesService, HelpersService $delpersService)
    {
        $this->directoriesService = $directoriesService;
        $this->helpersService = $delpersService;
        $this->arraySubjectOfRequests = array();
        $initData = $this->getClearTopic();
        $this->arrayResultConsult = $this->getClearResultConsult();
        $this->fillingData($initData);
    }


    public function getReportSubjectOfRequests($request)
    {
        $accessAdmin = (int) config('enums.ACCESS_ADMIN');
        $accessControllerOpfr = (int) config('enums.ACCESS_USER_CONTROLLER_OPFR');
        $access = (int) $request->access;

        $dateTimeFrom = $this->helpersService->getDataTimeShamp($request->dateFrom, 'from');
        $dateTimeTo = $this->helpersService->getDataTimeShamp($request->dateTo, 'to');
        $codesUpfr = $this->directoriesService->getCodesUpfr($request->access, $request->codeUpfr);

        $codeOpfr = config('enums.CODE_OPFR');

        //получим и заполним по темам данные
        $rawDataModul = DB::table('subtopics')
            ->select(DB::raw('idtopic,topics.topic as nameTopic,code,subtopics.subtopic as nameSubTopic, COUNT(calls.id) AS count'))
            ->leftJoin('calls', 'calls.subtopic', '=', 'subtopics.id')
            ->leftJoin('topics', 'subtopics.idtopic', '=', 'topics.id')
            ->whereBetween('calls.time_start', [$dateTimeFrom, $dateTimeTo])
            ->groupBy('calls.topic', 'subtopics.code');
        if (in_array($access, [$accessControllerOpfr, $accessAdmin])) {
            $rawData = $rawDataModul->get();
        } else {
            $rawData = $rawDataModul->whereIn('calls.kod_upfr', $codesUpfr)->get();
        }

        $this->fillingData($rawData);

        //получим и заполним кол-во вида консультации
        $typeConsultModul = DB::table('calls')
            ->select(DB::raw('typeconsult, typeconsultation.type as nameType,COUNT(calls.id) AS count'))
            ->leftJoin('typeconsultation', 'calls.typeconsult', '=', 'typeconsultation.id')
            //->whereIn('kod_upfr', $codesUpfr)
            ->whereBetween('time_start', [$dateTimeFrom, $dateTimeTo])
            ->groupBy('typeconsult');


        if (in_array($access, [$accessControllerOpfr, $accessAdmin])) {
            $typeConsult = $typeConsultModul->get();
        } else {
            $typeConsult = $typeConsultModul->whereIn('kod_upfr', $codesUpfr)->get();
        }

        $this->arraySubjectOfRequests['typeConsult'][1] = 0;
        $this->arraySubjectOfRequests['nameConsult'][1] = '';
        $this->arraySubjectOfRequests['typeConsult'][2] = 0;
        $this->arraySubjectOfRequests['nameConsult'][2] = '';
        foreach ($typeConsult as $key => $dataSubjectOfRequests) {
            $this->arraySubjectOfRequests['typeConsult'][$dataSubjectOfRequests->typeconsult] = $dataSubjectOfRequests->count;
            $this->arraySubjectOfRequests['nameConsult'][$dataSubjectOfRequests->typeconsult] = $dataSubjectOfRequests->nameType;
        }
        //получим и заполним результат консультирования
        $resultCosultDataModul = DB::table('calls')
            ->select(DB::raw('calls.resultconsult as id,resultconsultation.result as nameResult, COUNT(calls.id) AS count'))
            ->leftJoin('resultconsultation', 'resultconsultation.id', '=', 'calls.resultconsult')
            //->whereIn('kod_upfr', $codesUpfr)
            ->whereBetween('time_start', [$dateTimeFrom, $dateTimeTo])
            ->groupBy('resultconsult');

        if (in_array($access, [$accessControllerOpfr, $accessAdmin])) {
            $resultCosultData = $resultCosultDataModul->get();
        } else {
            $resultCosultData = $resultCosultDataModul->whereIn('kod_upfr', $codesUpfr)->get();
        }

        $this->fillingDataResultConsult($resultCosultData);
        if (in_array($access, [$accessControllerOpfr, $accessAdmin])) {
            $this->arraySubjectOfRequests['totalCalls'] = $this->getTotalCountRecords($dateTimeFrom, $dateTimeTo)->where('calls.kod_upfr', $codeOpfr)->count();
            $this->arraySubjectOfRequests['totalCallsHotLine'] = $this->getTotalCountRecords($dateTimeFrom, $dateTimeTo)->where('calls.kod_upfr', '!=', $codeOpfr)->count();
        } else {
            $this->arraySubjectOfRequests['totalCalls'] = '';
            $this->arraySubjectOfRequests['totalCallsHotLine'] = $this->getTotalCountRecords($dateTimeFrom, $dateTimeTo)->whereIn('kod_upfr', $codesUpfr)->count();
        }

        return  $this->arraySubjectOfRequests;
    }

    private function getTotalCountRecords($dateTimeFrom, $dateTimeTo)
    {
        return DB::table('calls')->whereBetween('time_start', [$dateTimeFrom, $dateTimeTo]);
    }

    private function getClearTopic()
    {
        return DB::table('subtopics')
            ->select(DB::raw('idtopic,topics.topic as nameTopic,code,subtopics.subtopic as nameSubTopic,0 as count'))
            ->leftJoin('topics', 'subtopics.idtopic', '=', 'topics.id')
            ->get();
    }

    private function getClearResultConsult()
    {
        $rawData = DB::table('resultconsultation')
            ->select(DB::raw('id,result as nameResult,0 as count'))
            ->get();
        $this->fillingDataResultConsult($rawData);
    }

    private function fillingData($rawData)
    {
        $this->arraySubjectOfRequests['totalCalls'] = 0;

        $arrayNameSubTopic = array();

        foreach ($rawData as $key => $data) {
            $this->arraySubjectOfRequests[$data->idtopic][$data->code] = 0;
        }
        foreach ($rawData as $key => $data) {
            $this->arraySubjectOfRequests[$data->idtopic][$data->code] += $data->count;
            $this->arraySubjectOfRequests['nameTopic'][$data->idtopic] = $data->nameTopic;
            $arrayNameSubTopic[$data->idtopic][$data->code] = $data->nameSubTopic;
            //$this->arraySubjectOfRequests['totalCalls'] += $data->count;
        }

        if (!isset($this->arraySubjectOfRequests['nameSubTopic'])) {
            $this->arraySubjectOfRequests['nameSubTopic'] = $arrayNameSubTopic;
        }
    }


    private function fillingDataResultConsult($rawData)
    {
        foreach ($rawData as $key => $data) {
            $this->arraySubjectOfRequests['resultConsultation'][$data->id] = $data->count;
            $this->arraySubjectOfRequests['nameResultConsultation'][$data->id] = $data->nameResult;
        }
    }
}
