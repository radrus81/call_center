<?php

namespace App\Services\Reports;

use App\Services\Calls\GetCallsService;
use App\Services\Reports\GetTopicStatisticService;
use App\Services\Reports\GetStatisticUsersService;
use App\Services\Reports\GetSecretWordStatisticService;
use App\Services\Reports\GetReportJournalPhoneService;
use App\Services\Reports\GetReportComplaintService;
use App\Services\Reports\GetReportSubjectOfRequestsService;

class ReportsService
{

    public function __construct(
        GetCallsService $getCallsService,
        GetTopicStatisticService $getTopicStatisticService,
        GetStatisticUsersService $getStatisticUsersService,
        GetSecretWordStatisticService $getSecretWordStatisticService,
        GetReportJournalPhoneService $getReportJournalPhoneService,
        GetReportComplaintService $getReportComplaintService,
        GetReportSubjectOfRequestsService $getReportSubjectOfRequestsService

    ) {
        $this->getCallsService = $getCallsService;
        $this->getTopicStatisticService = $getTopicStatisticService;
        $this->getStatisticUsersService = $getStatisticUsersService;
        $this->getSecretWordStatisticService = $getSecretWordStatisticService;
        $this->getReportJournalPhoneService = $getReportJournalPhoneService;
        $this->getReportComplaintService = $getReportComplaintService;
        $this->getReportSubjectOfRequestsService = $getReportSubjectOfRequestsService;
    }

    public function getDataForExcel($request)
    {
        switch ($request->typeReport) {
            case 'printJournal':
                return $this->getCallsService->getDataCalls($request);
            case 'oldStatistic':
            case 'newStatistic':
                return $this->getTopicStatisticService->getTopicStatistic($request);
            case 'statisticUsers':
                return $this->getStatisticUsersService->getStatisticUsers($request);
            case 'secretWordStatistic':
                return $this->getSecretWordStatisticService->getSecretWordStatistic($request);
            case 'reportJournalPhone':
                return $this->getReportJournalPhoneService->getReportJournalPhone($request);
            case 'complaint':
                return $this->getReportComplaintService->getReportComplaint($request);
            case 'reportSubjectOfRequests':
                return $this->getReportSubjectOfRequestsService->getReportSubjectOfRequests($request);
            default;
        }
    }
}
