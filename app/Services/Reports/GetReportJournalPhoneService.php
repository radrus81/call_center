<?php

namespace App\Services\Reports;

use Illuminate\Support\Facades\DB;
use App\Services\HelpersService;
use App\Services\DirectoriesService;



class GetReportJournalPhoneService
{

    public function __construct(DirectoriesService $directoriesService, HelpersService $helpersService)
    {
        $this->directoriesService = $directoriesService;
        $this->helpersService = $helpersService;
    }


    public function getReportJournalPhone($request)
    {
        $dateTimeFrom = $this->helpersService->getDataTimeShamp($request->dateFrom, 'from');
        $dateTimeTo = $this->helpersService->getDataTimeShamp($request->dateTo, 'to');
        $codesUpfr = $this->directoriesService->getCodesUpfr($request->access, $request->codeUpfr);

        DB::select('set @cnt=0;');

        return DB::table('calls')
            ->select(DB::raw('@cnt:=@cnt+1 as num,
                    calls.kod_upfr as codeUpfr,
                    DATE_FORMAT(FROM_UNIXTIME(calls.time_start),"%d.%m.%Y %H:%i:%s") as dateStart,
                    concat(calls.fam," ",calls.name," ",calls.otch) as fioClient,
                    calls.snils,
                    topics.topic as topicNewReport,
                    subtopics.subtopic,
                    typeconsultation.type as typeConsultation,
                    resultconsultation.result as resultConsultation,
                    FLOOR(1+(RAND()*5)) as durationCall,
                    users.fio as nameUser,
                    if (calls.kod_upfr=' . env('CODE_OPFR', 83) . ',1,2) as codeUser
            '))
            ->leftJoin('users', 'users.id', 'calls.user')
            ->leftJoin('topics', 'topics.id', 'calls.topic')
            ->leftJoin('subtopics', 'subtopics.id', 'calls.subtopic')
            ->leftJoin('typeconsultation', 'typeconsultation.id', 'calls.typeconsult')
            ->leftJoin('resultconsultation', 'resultconsultation.id', 'calls.resultconsult')
            ->whereIn('calls.kod_upfr', $codesUpfr)
            ->whereBetween('calls.time_start', [$dateTimeFrom, $dateTimeTo])
            ->get();
    }
}
