<?php

namespace App\Services\Reports;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Services\HelpersService;
use App\Services\DirectoriesService;



class GetTopicStatisticService
{

    public function __construct(DirectoriesService $directoriesService, HelpersService $helpersService)
    {
        $this->directoriesServise = $directoriesService;
        $this->helpersServise = $helpersService;
    }


    public function getTopicStatistic($request)
    {

        $dateTimeFrom = $this->helpersServise->getDataTimeShamp($request->dateFrom, 'from');
        $dateTimeTo = $this->helpersServise->getDataTimeShamp($request->dateTo, 'to');
        $codesUpfr = $this->directoriesServise->getCodesUpfr($request->access, $request->codeUpfr);

        $tableName = 'subtopics';
        $tableField = '.subtopic';
        $callsField = 'subtopic';
        if ($request->typeReport === 'oldStatistic') {
            $tableName = 'tema';
            $tableField = '.name';
            $callsField = 'tema';
        }

        $rawData = DB::table($tableName)
            ->select(DB::raw($tableName  . $tableField . ' as topic,upfr.id_upfr as codeUpfr,count(calls.id) as count '))
            ->crossJoin('upfr')
            ->leftjoin('calls', function ($join) use ($tableName, $callsField, $dateTimeFrom, $dateTimeTo) {
                $join->on($tableName . '.id', 'calls.' . $callsField)
                    ->where(function ($query) use ($dateTimeFrom, $dateTimeTo) {
                        $query->whereBetween('calls.time_start', [$dateTimeFrom, $dateTimeTo]);
                    });
                $join->on('calls.kod_upfr', 'upfr.id_upfr');
            })
            ->whereIn('upfr.id_upfr', $codesUpfr)
            ->groupBy($tableName  . $tableField, 'upfr.id_upfr')
            ->get();

        return $this->preparationData($rawData);
    }

    private function preparationData($rawData)
    {
        $statisticUpfrAll = array();
        $statisticReady = array();

        foreach ($rawData as $key => $val) {
            $statisticUpfrAll[$val->topic][$val->codeUpfr] = $val->count;
        }
        foreach ($statisticUpfrAll as $key => $val) {
            $statisticOneTopic = array();
            $statisticOpfr = array();
            foreach ($val as $key2 => $val2) {
                if ($key2 !== 83) {
                    $statisticOneTopic['upfr_' . $key2] = $val2;
                } else {
                    $statisticOpfr['opfr_' . $key2] = $val2;
                }
            }
            $statisticOpfr['topicStat'] = $key;

            $statisticReady[] = array_merge(array_reverse($statisticOpfr), $statisticOneTopic);
        }
        return $statisticReady;
    }
}
