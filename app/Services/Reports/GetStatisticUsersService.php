<?php

namespace App\Services\Reports;

use Illuminate\Support\Facades\DB;
use App\Services\HelpersService;
use App\Services\DirectoriesService;



class GetStatisticUsersService
{
    public function __construct(DirectoriesService $directoriesService, HelpersService $helpersService)
    {
        $this->directoriesService = $directoriesService;
        $this->helpersService = $helpersService;
    }


    public function getStatisticUsers($request)
    {
        return $this->getDataStatisticUsers($request);
    }


    private function getDataStatisticUsers($request)
    {
        $dateTimeFrom = $this->helpersService->getDataTimeShamp($request->dateFrom, 'from');
        $dateTimeTo = $this->helpersService->getDataTimeShamp($request->dateTo, 'to');
        $codeUpfrForReport = $request->codeUpfrForReport;

        $tableName = 'subtopics';
        $tableField = '.subtopic';
        $callsField = 'subtopic';
        if ($request->reportRadioTypeTopic === 'oldTopic') {
            $tableName = 'tema';
            $tableField = '.name';
            $callsField = 'tema';
        }
        $dataStatistic = DB::table($tableName)
            ->select(DB::raw($tableName  . $tableField . ' as topic,users.fio,count(users.id) as count '))
            ->leftjoin('calls', function ($join) use ($tableName, $callsField, $dateTimeFrom, $dateTimeTo) {
                $join->on($tableName . '.id', 'calls.' . $callsField)
                    ->where(function ($query) use ($dateTimeFrom, $dateTimeTo) {
                        $query->whereBetween('calls.time_start', [$dateTimeFrom, $dateTimeTo]);
                    });
            })
            ->leftjoin('users', function ($join) use ($codeUpfrForReport) {
                $join->on('users.id', 'calls.user')
                    ->where(function ($query) use ($codeUpfrForReport) {
                        $query->where('users.kod_upfr', '=', $codeUpfrForReport);
                        $query->whereIn('users.dostup', [config('enums.ACCESS_USER_OPFR'), config('enums.ACCESS_USER_UPFR_ALL')]);
                    });
            })
            ->orWhereNull('calls.tema')
            ->orWhereNull('users.id')
            ->orWhere('users.isBlocked', '=', 0)
            ->groupBy($tableName . $tableField, 'users.fio')
            ->get();

        return $this->convertDataToDataForExcel($request, $dataStatistic);
    }

    private function convertDataToDataForExcel($request, $dataStatistic)
    {
        $topics = $this->getTopics($request->reportRadioTypeTopic);
        $users = $this->getUsers($request->codeUpfrForReport);

        $haveData = array();

        foreach ($dataStatistic as $index => $value) {
            $haveData[$value->topic][$value->fio] = $value->count;
        }

        $readyDataForExcel = array();
        $itogoRow = array('topicStat' => 'Итого');

        foreach ($topics as $index => $valTopic) {
            $dataOnlyForTopic = array();
            $sumCol = 0;
            foreach ($users as $index => $valUser) {
                $dataOnlyForTopic['topicStat'] = $valTopic->topics;
                if (array_key_exists($valUser->fio, $haveData[$valTopic->topics])) {
                    $dataOnlyForTopic[$valUser->fio] =  $haveData[$valTopic->topics][$valUser->fio];
                    $sumCol += $dataOnlyForTopic[$valUser->fio];
                    //формируем итого
                    if (array_key_exists($valUser->fio, $itogoRow)) {
                        $itogoRow[$valUser->fio] += (int) $haveData[$valTopic->topics][$valUser->fio];
                    } else {
                        $itogoRow[$valUser->fio] = (int) $haveData[$valTopic->topics][$valUser->fio];
                    }
                } else {
                    $dataOnlyForTopic[$valUser->fio] = 0;
                    if (!array_key_exists($valUser->fio, $itogoRow)) {
                        $itogoRow[$valUser->fio] = 0;
                    }
                }
            }
            $dataOnlyForTopic['итого'] = $sumCol;
            $readyDataForExcel[] = $dataOnlyForTopic;
        }
        $readyDataForExcel[] = $itogoRow;
        return $readyDataForExcel;
    }



    private function getTopics($typeTopic)
    {
        if ($typeTopic === 'newTopic') {
            return DB::table('subtopics')
                ->select(DB::raw('subtopic as topics'))
                ->get();
        } else {
            return DB::table('tema')
                ->select(DB::raw('name as topics'))
                ->get();
        }
    }

    private function getUsers($codeUpfr)
    {
        return DB::table('users')
            ->select(DB::raw('fio'))
            ->where([
                ['kod_upfr', '=', $codeUpfr],
                ['isBlocked', '=', 0]
            ])
            ->whereIn('dostup', [config('enums.ACCESS_USER_OPFR'), config('enums.ACCESS_USER_UPFR_ALL')])
            ->get();
    }
}
