<?php

namespace App\Services\Reports;

use Illuminate\Support\Facades\DB;
use App\Services\HelpersService;
use App\Services\DirectoriesService;



class GetReportComplaintService
{
    public function __construct(DirectoriesService $directoriesService, HelpersService $helpersService)
    {
        $this->directoriesService = $directoriesService;
        $this->helpersService = $helpersService;
    }

    public function getReportComplaint($request)
    {
        $dateTimeFrom = $this->helpersService->getDataTimeShamp($request->dateFrom, 'from');
        $dateTimeTo = $this->helpersService->getDataTimeShamp($request->dateTo, 'to');

        if ($request->reportСomplaint === 'journalСomplaint') {
            DB::select('set @cnt=0;');
            $jurnal = DB::table('calls')
                ->select(DB::raw('@cnt:=@cnt+1 as num,
                            upfr.name_upfr as nameUpfr,
                            DATE_FORMAT(calls.dateOfIssue,"%d.%m.%Y") as dateOfIssue,
                            calls.complaintDescription
                        '))
                ->leftJoin('upfr', 'upfr.id_upfr', 'calls.complaintСodeUpfr')
                ->whereBetween('calls.time_start', [$dateTimeFrom, $dateTimeTo])
                ->where('calls.complaintСodeUpfr', '!=', 0)
                ->get();
            if (!count($jurnal)) {
                return array((object) array('num' => 0, 'nameUpfr' => 'нет данных', 'dateOfIssue' => 'нет данных', 'complaintDescription' => 'нет данных'));
            } else {
                return $jurnal;
            }
        } else {
            return $this->getStatisticСomplaint($dateTimeFrom, $dateTimeTo);
        }
    }

    private function getStatisticСomplaint($dateTimeFrom, $dateTimeTo)
    {

        $allUpfr = $this->getNamesUpfr();
        $rawData = DB::table('calls')
            ->select(DB::raw('upfr.name_upfr as nameUpfr, count(calls.complaintСodeUpfr) as count'))
            ->leftJoin('upfr', 'upfr.id_upfr', 'calls.complaintCodeUpfr')
            ->whereBetween('calls.time_start', [$dateTimeFrom, $dateTimeTo])
            ->where([['calls.complaintСodeUpfr', '!=', 0], ['upfr.id_upfr', '!=', 83]])
            ->groupBy('calls.complaintСodeUpfr')
            ->get();

        foreach ($allUpfr as $key => $upfr) {
            $tempArray = array();
            $isUpfrInRawData = $this->checkIsUpfrInReport($upfr->name_upfr, $rawData);
            if (!$isUpfrInRawData) {
                $tempArray['nameUpfr'] = $upfr->name_upfr;
                $tempArray['count'] = 0;
                $rawData[] = (object) $tempArray;
            }
        }
        return $rawData;
    }

    private function checkIsUpfrInReport($nameUpfrForCheck, &$rawData)
    {

        if (count($rawData)) {
            foreach ($rawData as $key => $upfr) {
                $nameUpfr = $upfr->nameUpfr;
                if ($nameUpfr === $nameUpfrForCheck) {
                    return true;
                }
            }
        }
        return false;
    }

    private function getNamesUpfr()
    {
        return DB::table('upfr')
            ->select(DB::raw('name_upfr'))
            ->where('upfr.id_upfr', '!=', 83)
            ->get();
    }
}
