<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class LoginController extends Controller
{

    public function checkLogin(Request $request)
    {
        $where = array();
        $credentials = $request->only('login', 'password');
        $isAuthorizationDomen = env('AUTHORIZATION_DOMEN', false);

        if ($isAuthorizationDomen) {
            $login = $credentials['login'] . env('DOMEN_NAME', '');
            $password = $credentials['password'];
            $ldap = ldap_connect(env('DOMEN_IP', ''), env('DOMEN_PORT', '389'));
            if (!$ldap) {
                return response()->json([
                    'errorMessage' => 'Не удалось подключиться к домену!',
                ], 200);
            }
            ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);
            $bind = @ldap_bind($ldap, $login, $password);
            if (!$bind) {
                return response()->json([
                    'errorMessage' => 'Неправильный логин или пароль к домену!',
                ], 200);
            } else {
                $where[] = ['users.login', '=', $credentials['login']];
            }
        } else {
            $login = $credentials['login'];
            $password = md5($credentials['password']);
            $where[] = ['users.login', '=', $login];
            $where[] = ['users.pass', '=', $password];
        }


        $user = DB::table('users')->select(
            'users.id as idUser',
            'users.login',
            'users.fio',
            'users.kod_upfr',
            'users.dostup as access',
            'users.codeuser as codeUser',
            'upfr.name_upfr as nameupfr',
            'dostup.name_dostup as descAccess',
            'users.isBlocked'

        )
            ->join('upfr', 'users.kod_upfr', '=', 'upfr.id_upfr')
            ->join('dostup', 'users.dostup', '=', 'dostup.id_dostup')
            ->where($where)
            ->first();

        $errorMessage = '';
        if (!$user && !$isAuthorizationDomen) {
            $errorMessage = 'Неправильный логин или пароль!';
        } elseif (!$user && $isAuthorizationDomen) {
            $errorMessage = 'Пользователь ' . $credentials['login'] . ' не зарегист. в программе.Обратитесь в ОЗИ.';
        } elseif ($user->isBlocked == 1) {
            $errorMessage = 'Пользователь ' . $credentials['login'] . ' заблокирован.Обратитесь в ОЗИ.';
        }
        if (!$errorMessage) {
            return response()->json([
                'user' => $user,
                'errorMessage' => false
            ], 200);
        } else {
            return response()->json([
                'errorMessage' => $errorMessage
            ], 200);
        }
    }
}
