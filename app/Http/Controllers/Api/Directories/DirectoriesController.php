<?php

namespace App\Http\Controllers\Api\Directories;

use App\Http\Controllers\Controller;
use App\Services\DirectoriesService;


class DirectoriesController extends Controller
{

    public function __construct(DirectoriesService $directoriesService)
    {
        $this->directoriesService = $directoriesService;
    }

    public function getDirectories($access, $codeUpfr)
    {

        return response()->json([
            'users' => $this->directoriesService->getUsers($access, $codeUpfr),
            'areaOfDislocation' =>  $this->directoriesService->getAreaOfDislocation(),
            'counterparties' =>  $this->directoriesService->getCounterparties(),
            'oldTopic' =>  $this->directoriesService->getOldTopic(),
            'topics' =>  $this->directoriesService->getTopics(),
            'subTopics' =>  $this->directoriesService->getSubTopics(),
            'typeConsultation' =>  $this->directoriesService->getTypeConsultation(),
            'resultConsultation' =>  $this->directoriesService->getResultConsultation(),
            'upfr' =>  $this->directoriesService->getUpfr($access, $codeUpfr),
            'status' => $this->directoriesService->getStatuses()
        ]);
    }
}
