<?php

namespace App\Http\Controllers\Api\Calls;

use App\Http\Controllers\Controller;
use App\Services\Calls\GetCallsService;


class UserCallsController extends Controller
{

    public function __construct(GetCallsService $getCallsService)
    {
        $this->getCallsService = $getCallsService;
    }

    public function getUserCalls($idUser, $access)
    {
        return response()->json([
            'userStatistic' => $this->getCallsService->getUserStatistic($idUser, $access),
            'userLastCalls' => $this->getCallsService->getUserLastCalls($idUser)
        ], 200);
    }
}
