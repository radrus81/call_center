<?php

namespace App\Http\Controllers\Api\Calls;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Services\Calls\GetCallsService;
use App\Services\Calls\InsertCallService;
use App\Services\Calls\UpdateCallService;


class CallsController extends Controller
{
    public function __construct(
        GetCallsService $getCallsService,
        InsertCallService $insertCallService,
        UpdateCallService $updateCallService
    ) {
        $this->getCallsService = $getCallsService;
        $this->insertCallService = $insertCallService;
        $this->updateCallService = $updateCallService;
    }

    public function getCalls(Request $request)
    {
        return response()->json([
            'calls' =>  $this->getCallsService->getDataCalls($request),
            'totalCountCalls' => $this->getCallsService->getTotalCountCalls($request),
            'totalCountCallsForCurrentDate' => $this->getCallsService->getTotalCountCallsForCurrentDate($request)
        ], 200);
    }

    public function addCall(Request $request)
    {
        $this->insertCallService->InsertCall($request);

        return response()->json([
            'userStatistic' => $this->getCallsService->getUserStatistic($request->idUser, $request->access),
            'userLastCalls' =>  $this->getCallsService->getUserLastCalls($request->idUser)
        ], 200);
    }


    public function updateCall(Request $request)
    {
        return response()->json([
            'resultUpdate' =>  $this->updateCallService->UpdateCall($request)
        ], 200);
    }

    public function deleteCall(Request $request)
    {
        $resultDelete = DB::table('calls')->where('id', '=', $request->id)->delete();
        return response()->json([
            'resultDelete' => $resultDelete
        ], 200);
    }

    public function changeStatusCall(Request $request)
    {
        DB::table('calls')->where('id', $request->id)
            ->update([
                'status' => $request->status
            ]);
        return response()->json([
            'userStatistic' => $this->getCallsService->getUserStatistic($request->idUser, $request->access)
        ], 200);
    }
}
