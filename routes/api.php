<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Api'], function () {

    Route::group(['namespace' => 'Auth'], function () {
        Route::post('login', 'LoginController@checkLogin');
    });

    Route::group(['namespace' => 'Directories'], function () {
        Route::get('directories/{access}/{codeUpfr}', 'DirectoriesController@getDirectories');
    });

    Route::group(['namespace' => 'Calls'], function () {
        Route::patch('addcall', 'CallsController@addCall');
    });

    Route::group(['namespace' => 'Calls'], function () {
        Route::get('calls', 'CallsController@getCalls');
    });

    Route::group(['namespace' => 'Calls'], function () {
        Route::get('usercalls/{idUser}/{access}', 'UserCallsController@getUserCalls');
    });
    Route::group(['namespace' => 'Calls'], function () {
        Route::patch('calls/updatecall/', 'CallsController@updateCall');
    });

    Route::group(['namespace' => 'Calls'], function () {
        Route::get('secretwords/{snils}', 'SecretWordController@getSecretWord');
    });

    Route::group(['namespace' => 'Calls'], function () {
        Route::patch('calls/changestatuscall/', 'CallsController@changeStatusCall');
    });

    Route::group(['namespace' => 'Calls'], function () {
        Route::delete('calls/', 'CallsController@deleteCall');
    });

    Route::group(['namespace' => 'Reports'], function () {
        Route::get('reports/', 'ReportsController@createReport');
    });

    Route::group(['namespace' => 'Users'], function () {
        Route::get('users/', 'UsersController@getDataFromTableUsers');
    });
    Route::group(['namespace' => 'Users'], function () {
        Route::get('users/upfrlist', 'UsersController@getUpfrList');
    });
    Route::group(['namespace' => 'Users'], function () {
        Route::post('/user', 'UsersController@addUser');
    });
    Route::group(['namespace' => 'Users'], function () {
        Route::put('/user', 'UsersController@updateUser');
    });

    Route::group(['namespace' => 'Settings'], function () {
        Route::get('/settings', 'SettingsController@getSettings');
    });

    Route::group(['namespace' => 'HistoryCalls'], function () {
        Route::get('/historycalls/{page}/{fullNamePensioner}', 'HistoryCallsController@getHistoryCalls');
    });
});
